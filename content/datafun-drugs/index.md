+++
title = "Datafun: Drugs"
date = 2014-10-20

[taxonomies]
categories = ["blog"]
tags = ["datafun", "graphics"]
+++
{{ image(src="drugs.png", alt="drugs") }}

A binary table showing which of 24094 drugs (rows) containing which of the 4139 possible substances (columns). The color shows intensity when blurring vertically. Most common substances are marked.

<!-- more -->

Data source: [DailyMed](https://dailymed.nlm.nih.gov/dailymed/index.cfm)

This work was originally published as part of [my Bachelor thesis](@/pages/external/index.md).
