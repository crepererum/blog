+++
title = "Datafun: Flatmate Casting"
date = 2014-10-27

[taxonomies]
categories = ["blog"]
tags = ["datafun", "graphics"]
+++
{{ image(src="casting.png", alt="cover") }}

Casting number 7, I'm getting old here. My flat needed a new member again so we've written some text, collected some detail, prepared some pictures and uploaded a nice advertisement to [www.wg-gesucht.de](https://www.wg-gesucht.de/). Like every year, they spammed my inbox, but this year, it was worse. So many homeless students. After 15 days, when the ad was on the bottom of the list, I received 98 application emails. Here is a quick analysis of them.

<!-- more -->

Each half circle represents one application. Left circles are female applicants, right ones are male. It is obvious that my flat is located in Karlsruhe, a city of male technology students.

The location depends on the time and day of the email. The y dimension represents time of the day from top as midnight to middle as noon and bottom as the second before midnight. To complete the time diagram the x axis represents the day, which resulted in these discrete locations. As you can see, my inbox got flooded during the first hours, the frequency decreases over time and people are sleeping during the night and don't write emails in the morning.

The area of the circles (or the squared radius) is proportional to the number of words in the application email. The length of the mails ranges from 429 down to 15. I'm wondering how this lazy guy wants to get an invitation, keeping the situation in Karlsruhe in mind.

The colour of the circles is calculated using the [HSV colour model](https://en.wikipedia.org/wiki/HSL_and_HSV). The saturation is set to 1 whereas the hue part depends on the origin of the applications. Calculating this is rather simple and just represents the logarithmic distance between the application's country centre and the centre of Germany. Here centre refers to the geographic centre of the country. So red circles are likely to represent German applicants. Pink circles represent people with unknown origin.

The brightness/value of the circles is proportional to the age, the youngest people having the highest brightness -- old people tend to be less colourful, you know. If the age is unknown I assumed that there are pretty old, because can someone forget to write down here/his own age?! We were looking for people from 19 to 27 years, which more or less is represented by the people (range from 18 to 29 years).

By the way: The upper left violet circle is Theo, our new flat member. As you can see he is a night owl and didn't write so many words.

Data source: My inbox, [WolframAlpha](https://www.wolframalpha.com/)
