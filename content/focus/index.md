+++
title = "Focus!"
date = 2014-05-17

[taxonomies]
categories = ["blog"]
tags = ["design"]
+++
During the restyle of [brain4D](https://crepererum.github.io/brain4D) I tried to style the UI more intuitive. One of the main points was to clarify which parts are active and which are not. The main approach is the variance of colors, so that the contrast of outer background, border, inner background and text is high or low.

<!-- more -->

See this example:
<div class="tom-focus-form">
	<div class="tom-focus-button">Default</div>
    <div class="tom-focus-button tom-focus-active">Focus</div>
    <div class="tom-focus-button tom-focus-passive">Passive</div>
</div>

This kindly tries to simulate the focus on light and shadow in the real world. But focusing is more. It's about depth and blurring and sharpness. Because [web technology isn't ready for blurring](https://caniuse.com/#feat=css-filters), we can use a trick. There are colored and blurred shadows around text and boxes. If we adjust the color right, we can archive focus and defocus:
<div class="tom-focus-form">
	<div class="tom-focus-button">Default</div>
    <div class="tom-focus-button tom-focus-active2">Focus</div>
    <div class="tom-focus-button tom-focus-passive2">Passive</div>
</div>

Nice. Notice how the high contrast and sharpness of the focus object guides the eye. Finally let's test this in a real world szenario. Imagine a user registration request that got rejected because of an invalid email address:
<div style="text-align:center;">
<div class="tom-focus-eform">
	<div class="tom-focus-etitle">New Member</div>
	<div class="tom-focus-eline tom-focus-epassive"><div class="tom-focus-ename">First Name:</div><input type="text" value="Tom" /></div>
    <div class="tom-focus-eline tom-focus-epassive"><div class="tom-focus-ename">Last Name:</div><input type="text" value="Fisher" /></div>
    <div class="tom-focus-eline tom-focus-eactive"><div class="tom-focus-ename">Email:</div><input type="text" value="tom@foo.d" /><div class="tom-focus-emessage">Invalid</div></div>
    <div class="tom-focus-eline tom-focus-epassive"><div class="tom-focus-ename">Hometown:</div><input type="text" value="Far far away" /></div>
    <div class="tom-focus-eline tom-focus-epassive"><div class="tom-focus-ename">Promoter:</div><input type="text" value="Dude" /></div>
    <div class="tom-focus-esubmit">Submit</div>
</div>
</div>

## Technical Details
The implementation uses only CSS, so it's ready for mobile devices and retina displays. Apart from changing the [text color](https://www.w3schools.com/cssref/pr_text_color.asp), [border](https://www.w3schools.com/cssref/pr_border.asp) and [background](https://www.w3schools.com/cssref/pr_background-color.asp), the true magic are [text-shadow](https://www.w3schools.com/cssref/css3_pr_text-shadow.asp) and [box-shadow](https://www.w3schools.com/cssref/css3_pr_box-shadow.asp). They are centered and have a 2px blur radius. The color depends on focus or defocus. Defocused elements have the same shadow color as their own text (text-shadow), their background (box-shadow) or if there is a border, border color (box-shadow, ignore the background color). Focused elements use a decent contrast color. In a light environment (light gray, no white), use an even lighter shadow color. In dark environments (dark gray, you don't use black, I hope), use an even darker shadow color. Because it's not a complementary color, our brain don't separate it. If you want to see some code, just debug this page. The CSS style element is right below this text block.

<style>
	.tom-focus-active {
    	border: 1px solid #aaa !important;
    	background: #171717 !important;
    	color: #aaa !important;
    }
    .tom-focus-passive {
    	border: 1px solid #555 !important;
        background: #272727 !important;
    	color: #555 !important;
    }
    .tom-focus-active2 {
    	border: 1px solid #aaa !important;
        box-shadow: 0 0 2px #000;
    	background: #171717 !important;
    	color: #aaa !important;
        text-shadow: 0 0 2px #000;
    }
    .tom-focus-passive2 {
    	border: 1px solid #555 !important;
        background: #272727 !important;
        box-shadow: 0 0 2px;
    	color: #555 !important;
        text-shadow: 0 0 2px;
    }
	.tom-focus-button {
    	background: #111;
        border: 1px solid #888;
        border-radius: 4px;
    	color: #888;
        display: inline-block;
        font-weight: bold;
        padding: 10px;
    }
    .tom-focus-form {
    	background: #222;
        border-radius: 10px;
        margin-bottom: 40px;
        padding: 20px;
    }
    .tom-focus-eform {
    	background: #eee;
        border-radius: 10px;
        color: #444;
        display: inline-block;
        margin-bottom: 40px;
        padding: 20px;
        text-align: left;
        width: 600px;
    }
    .tom-focus-eform input {
    	background: white;
        border: 0;
        box-sizing: border-box;
        padding: 5px;
    	width: 340px;
    }
    .tom-focus-emessage {
	    box-sizing: border-box;
    	display: inline-block;
        font-weight: bold;
        text-align: center;
        padding: 5px 20px;
        width: 120px;
    }
    .tom-focus-ename {
    	box-sizing: border-box;
    	display: inline-block;
        padding-right: 20px;
        text-align: right;
        width: 140px;
    }
    .tom-focus-eactive {
    	color: #f22;
        text-shadow: 0 0 4px #fcc;
    }
    .tom-focus-eactive input {
    	box-shadow: 0 0 2px #ddd;
        color: #f22;
        text-shadow: 0 0 4px #fcc;
    }
    .tom-focus-eactive .tom-focus-emessage {
    	background: #fcc;
        box-shadow: 0 0 2px #fcc;
    }
    .tom-focus-epassive {
    	color: #bbb;
    	text-shadow: 0 0 2px;
    }
    .tom-focus-epassive input {
    	background: #f7f7f7;
    	box-shadow: 0 0 2px #f7f7f7;
        color: #bbb;
        text-shadow: 0 0 2px;
    }
    .tom-focus-esubmit {
    	background: #9bd;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
        font-weight: bold;
        margin-top: 20px;
        text-align: center;
        padding: 5px;
    }
    .tom-focus-etitle {
    	font-weight: bold;
        margin-bottom: 20px;
        text-align: center;
    }
</style>
