+++
title = "Ransomware – Not a single click away"
date = 2024-12-07

[taxonomies]
categories = ["blog"]
tags = ["tech", "security"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

Ransomware has become an increasing thread to companies. <abbr>IT</abbr> departments often suggest that clicking a link in a fishing mail can infect you and that it is the personal responsibility of each individual employee to not do that. I think this framing is flawed. There a many measures that an organization can and should implement that each will protect against "single click infections". Manual checking of links should never be your only layer of defense and blaming individuals is just a cheap excuse.

It is unlikely that many companies implement all defenses proposed here, but I think it is important to keep in mind that we as an industry can and should do better. Ransomware attacks are -- at least to a high degree -- avoidable.

<!-- more -->

# Inbox ≠ Kitchen Sink
Most companies use one email account per person. There they find everything -- important company updates, GitHub notifications, reminders for their upcoming travels, "we collect money for this person's birthday", the latest brain melting idea the founder had while taking a shit, communication with customers, spam, fishing mails, ... <abbr>IT</abbr> departments then tell people to carefully check every sender and every link they click on. If one would do that for every single mail, I bet that no real work would get done in most companies. Instead of relying on manual labor of humans, the inbox should be redesigned as follows:

- **Inbox Split:** Separate the inbox into at levels of trust, e.g. "Internal", "trusted 3rd parties" (e.g. GitHub, your travel agency), "known contacts" (e.g. customers you have contacted before), "untrusted" (everything else minus detected threads, these should never be accessible from a normal interface). Make these trust levels very clear -- e.g. via color coding -- and do not mix them into a single list. This also helps people prioritizing what to read first.
- **Security Checks:** Check incoming emails by the latest security standards. Was the data transferred via <abbr>[TLS]</abbr> and <abbr>[DNSSEC]</abbr>? Did the mail pass <abbr>[DKIM]</abbr>, <abbr>[DMARC]</abbr>, <abbr>[SPF]</abbr>, and <abbr>[ARC]</abbr>? Try to roll of that into a simple flag, ideally even filter out non-compliant emails. Gmail for example does that, but the flag is somewhat hidden.
- **End-to-end Encryption & Signing:** While the setup is rather cumbersome, <abbr>[PGP]</abbr> and <abbr>[S/MIME]</abbr> not only ensure that mails are encrypted, but also that you can verify the sender. The infrastructure and integration should be provided by the <abbr>IT</abbr> department and should be a no-brainer for the individual.
- **Typo-Detection:** Detect mails from and to accounts that are nearly identical to "known contacts". This is rather tricky for humans to spot, but easy for machines.
- **Fleet Protection:** Organization-wide spam and fishing detection can catch the bigger attacks and annoyances. Combine that with public lists for bad actors. This insight is easier to gain from a fleet level than from an individuals point of view.
- **Alternative Messengers:** When possible, do not use email but messengers that tend to be less broken. Examples are: [Matrix], [Mattermost], [Slack], [Threema], and [Zulip].

# Sandbox
Assume that your employees will download all sorts of wild stuff and build systems so that a single executable is sandboxed so well, that it cannot do any harm. To my knowledge, no operating system currently archives that.

- **Application Sandbox:** Applications should run in their own sandbox. That is standard for Android and iOS and somewhat works on desktop operating systems. By default, an application should have no access to any company or personal information. All permissions should be opt-in. This also holds for command line tooling, but there multiple tools might be grouped together. For Linux, see [Flatpak] and [Toolbox].
- **Proper <abbr>OS</abbr>:** The operating system should use [Secure Boot] and full disk encryption. Ideally it should also be immutable.
- **Write ^ Execute:** Software developers have long-established that memory (= <abbr>RAM</abbr> content) should be either writable or executable, but never both. For operating systems however, this is often violated. Even if you run an immutable system like [Fedora Atomic], people often have binaries in their home directories (e.g. via `{go, cargo} install`). Even if they have no binaries, tool configurations -- e.g. for shells -- are essentially executables. The goal should be that if you run `kubectl logs ...` and confirm the login action via your browser, you should be sure that you actually requested logs and don't just infect a whole production cluster.

# Privilege Minimization & Control
If a rouge process escapes the sandbox and gains credentials from an endpoint, the harm should be minimized:

- **Object Scope:** Credentials handed to endpoints and people should be tightly scoped, e.g. limited to a single cluster/system.
- **Time Scope:** Tokens should be short-lived. User should rather request them on demand.
- **Subject Scope:** Tokens should be bound to a single endpoint or individual. This may be enforced by hardware, see next point.
- **Hardware Tokens:** To generate session tokens, a hardware key should be required to make cloning & impersonation harder.
- **Token Logging:** Generated tokens should leave a log message in a cryptographic append-only log. The requester -- machine or person -- should leave a justification message with every request.
- **Action Logging:** Actions performed with tokens should be recorded. This is true for <abbr>REST</abbr> <abbr>API</abbr>s as well as for <abbr>SSH</abbr> sessions.
- **Two-person Rule:** It is unavoidable to have some high privileged tokens, e.g. for a cloud account admin. These should only be used in tandem of two people (or more).

# Backup Security
When ransomware actually hits your system, proper backups are important:

- **Full Backups:** Have backups of everything data bit that is relevant for your business, including configuration state. You may use "configuration as code".
- **Automated Backups:** Backups run automated in regular intervals, but at least daily.
- **Offsite Storage:** The backup data should be stored on a remote location. For cloud backups, that means that they should be stored in a different data center. That is also important for other forms of disasters like hurricanes or floods.
- **Testing:** Test your backup and recovery path regularly, ideally in an automated setup.
- **Automated Recovery:** Ensure that your recovery is as automated/scripted as possible. No institutional knowledge or book-long instruction manual should be required.
- **Append-only Backups:** The endpoint that creates the backup has a somewhat risky combination of access rights -- the endpoint's permissions plus access to the backup store. Make sure that the latter can only be used to append state, never to delete any state. That also means that new backups do not overwrite existing ones. Ideally you also limit the read permissions to the backup store to avoid exfiltration of data from that store.
- **Encrypt & Sign:** Your backups are encrypted, ideally using asymmetric cryptography (also see previous point). They are signed. Keys are backed up properly.
- **Multiple Snapshots:** You keep multiple backup versions. This is important in case you spot ransomware attacks late, and some backup versions are already compromised.
- **Efficiency:** To store backups cost-efficient and to speed up their creation process, you may use delta backups and compression. This may require that an endpoint can at least read its own previous backups. Decide if that is okay, otherwise use client-side state to calculate deltas.
- **Careful Deletion:** Eventually you want to delete old backup versions. Decide on a strategy that keeps older backups in coarser intervals and newer ones in more frequent intervals -- e.g. keep last 10 monthly backups, last 10 daily, last 10 hourly, ... The deletion system should be not be the endpoint that writes the backup and should be isolated carefully due to the risky credentials.


{{ image_credits(author_name="Reynaldo #brigworkz Brigantty", author_url="https://www.pexels.com/@rbrigant44/", img_url="https://www.pexels.com/photo/black-steel-pet-cage-with-one-dollar-747113/", site="Pexels") }}


[ARC]: https://en.wikipedia.org/wiki/Authenticated_Received_Chain
[DKIM]: https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail
[DMARC]: https://en.wikipedia.org/wiki/DMARC
[DNSSEC]: https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions
[Fedora Atomic]: https://fedoraproject.org/atomic-desktops/
[Flatpak]: https://flatpak.org/
[Matrix]: https://matrix.org/
[Mattermost]: https://mattermost.com/
[PGP]: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
[S/MIME]: https://en.wikipedia.org/wiki/S/MIME
[Secure Boot]: https://en.wikipedia.org/wiki/UEFI#Secure_Boot
[Slack]: https://slack.com/
[SPF]: https://en.wikipedia.org/wiki/Sender_Policy_Framework
[Threema]: https://threema.ch/
[TLS]: https://en.wikipedia.org/wiki/Transport_Layer_Security
[Toolbox]: https://containertoolbx.org/
[Zulip]: https://zulip.com/
