+++
title = "Defaults Matter"
date = 2025-02-27
description = "A potential security issue with Home Assistant and its matter bridge."

[taxonomies]
categories = ["blog"]
tags = ["tech", "security"]

[extra]
image="cover.jpeg"
+++
{{ image(src="cover.jpeg", alt="cover") }}

Last year our electricity bill was higher than expected. So I've decided to I need some insights into our consumption and bought a [<abbr>_LEDVANCE_</abbr> _Matter_ <abbr>SMART</abbr>+](https://shop.ledvance.com/products/smart-wifi-mat-plug-eu-wh-ledv) plug. It can connect to my [Home Assistant](https://www.home-assistant.io/) installation using [_matter_](https://csa-iot.org/all-solutions/matter/), the promising new standard for [IoT](https://en.wikipedia.org/wiki/Internet_of_things).

Since my Home Assistant setup runs self-managed on an [Orange Pi 5B](http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/details/Orange-Pi-5B.html), I opted to install [`python-matter-server`](https://github.com/home-assistant-libs/python-matter-server) in a container alongside the Home Assistant installation. During the setup I have encountered a potential security issue that I have reported and I will now share publicly.

<!-- more -->

# Advisory
Here is the advisory that I have filed on 2024-10-20. The [GitHub <abbr>URL</abbr>](https://github.com/home-assistant/core/security/advisories/GHSA-83h5-q4gx-cgwg) is **not** usable by the public since it was rejected/"closed".

## Summary
[`python-matter-server`](https://github.com/home-assistant-libs/python-matter-server) exposes read & write access to matter devices without authentication.

**While I am not 100% sure if this classifies as a security issue (esp. since the matter integration is marked as "beta"), just opening a public GitHub issue felt wrong. Better be safe than sorry. Feel free to move this to an ordinary issue. Thank you.**

## Details
The <abbr>README</abbr> suggests starting the `python-matter-server` container w/ `--network=host`, probably to avoid some IPv6 issues. However, this also means that -- if not prevented by a firewall -- the <abbr>UI</abbr> and [WebSocket](https://en.wikipedia.org/wiki/WebSocket) port (`5580`) is now exposed on the host and hence to all networks that this host sits on (if the host is exposed to the internet, that now means it's also on the internet). That port requires zero authentication. Home assistant -- which is the biggest user of `python-matter-server` -- requires authentication to shield dashboards (= read) and controls (= write) from unprivileged humans and machines (incl. malware). It seems that `python-matter-server` kinda pushes a hole into that setup for all Matter devices registered with that controller.

Ideally the <abbr>UI</abbr> and websocket port would require some form of authentication (e.g. a password). Note that support for that has to be added to home assistant.

As an alternative, we should at least advice users (e.g. in the <abbr>README</abbr>) that they should set some port blocking (e.g. via iptables or nftables) to prevent access external (as in: not from the same machine).

## PoC
### Local
1. **devices:** pick 3 devices
    - PoC _host_ (e.g. a raspberry pi)
    - a _matter device_
    - a local network _attacker_ (e.g. a developer laptop)
2. **set up:**
    - install home assistant on _host_
    - set up [ `python-matter-server` ](https://github.com/home-assistant-libs/python-matter-server) as described by its <abbr>README</abbr>
    - add matter integration to home assistant
    - pair _matter device_
3. **attack:**
    - access _host_ port `5580` from _attacker_, list matter device and attributes from <abbr>UI</abbr> (= read w/o auth)
    - use description from  `python-matter-server` <abbr>README</abbr> to perform changes to matter device via python (= write w/o auth)

### Global
A quick [Censys search](https://search.censys.io/search?resource=hosts&sort=RELEVANCE&per_page=25&virtual_hosts=EXCLUDE&q=services.port+%3D+5580+and+services.port+%3D+8123) that filters for port `5580` (`python-matter-server`) and port `8123` (Home Assistant, to increase the likelihood that the other port is the matter sever) found a few hosts where `python-matter-server` is indeed exposed to the public. This confirms the hypothesis that this is a real problem.

## Impact
Unauthenticated access.

# Response
The response was received on 2024-11-25:

> Thank you for bringing a security concern to our attention. However, it does not qualify as a security report on our end, as this requires a network that’s already seriously compromised.
>
> Generally, users should not expose ports to the internet, unless there's a specific need and the required precautions to secure the port has already been taken by the user.
>
> The only officially supported way of running the Matter Server with Home Assistant, is by using the Home Assistant Matter Server add-on. By default, for the Home Assistant Matter Server add-on, the websocket server is bound to an internal interface only and external connections are prohibited.
>
> There are no plans at the moment to add authentication to the Matter Server. Users that are running the Matter Server not via the Home Assistant Matter Server add-on, need to ensure that access to the used port is restricted/secured, eg by binding to localhost/host internal interface only.
>
> We will clarify the readme of the Matter Server regarding the securing of the used ports.
>
> We appreciate your efforts in helping us maintain the security of our project.
>
> Sincerely,
>
> \<retracted\>[^name]

# My Take
The response narrows the thread model of Home Assistant installations quite significantly by excluding network access. That is -- in my opinion -- a bit shortsighted. First, a "seriously compromised network" is not just a singular rating but comes in levels:

1. **Network Connection:** Being able to open new connections to network devices and use them to talk to services. This basically applies to most WiFi networks and also home setups once you either have the WiFi password of physical access to an [Ethernet](https://en.wikipedia.org/wiki/Ethernet) port. This is already sufficient to perform the described attack.
2. **[<abbr>MITM</abbr>](https://en.wikipedia.org/wiki/Man-in-the-middle_attack):** Being able to intercept and potentially modify network traffic. This may be possible by rouge devices in a home network using [<abbr>ARP</abbr> spoofing](https://en.wikipedia.org/wiki/ARP_spoofing) or [<abbr>NDP</abbr>](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol) spoofing, but is sometimes harder due to smart routers. Also note that this is generally **not** possible when the traffic goes through loopback network interface.

Second we should consider how the network may get "compromised":

- **Access Control Bypass:** The network is secure but that security was bypassed, e.g. somewhat guessed your WiFI password. This is probably rather unlikely.
- **Accidental Exposure:** An internal device or port was exposed to the internet by accident. As [shown in the advisory](#global), this may happen more often than anticipated, especially when the insecure service (e.g. the _matter_ server) is hosted on the same device that also exposes a valid public service (e.g. Home Assistant).
- **Hacked Device:** In vain of bad security practices -- especially for many [IoT](https://en.wikipedia.org/wiki/Internet_of_things) devices -- we have to consider the possibility that a network-local device may be compromised.

# Counter-Measures
The [current instructions for Docker](https://github.com/home-assistant-libs/python-matter-server/blob/a5fe46b3f2b89720c49d6d3dc859b4af06437a32/docs/docker.md) still do **not** mention the risk. I have secured my _matter_ installation by adding the following [nftables](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page) rules:

```
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority filter;
                iif != "lo" tcp dport 5580 drop;
        }
        chain forward {
                type filter hook forward priority filter;
        }
        chain output {
                type filter hook output priority filter;
        }
}
```

# The _Matter_ Experience
The _matter_ interface of the <abbr>_LEDVANCE_</abbr> plug [is horribly broken](https://github.com/home-assistant/core/issues/128522). Despite being from a German brand, it is just a cheap [white-label product](https://en.wikipedia.org/wiki/White-label_product) from some Chinese manufacturer. I have sent it back (for free) and replaced it via a [Tasmota Sockets by Eightree](https://eightreesmart.com/products/eightree-mini-smart-wlan-steckdose-mit-alexa-google-home-nur-2-4ghz-wifi-4er-pack?_pos=2&_sid=a33a885da&_ss=r). They are connected to Home Assistant using [<abbr>MQTT</abbr>](https://mqtt.org/) and work great. They are also cheaper than the <abbr>_LEDVANCE_</abbr> plugs.

I still want _matter_ to succeed though because it promises to be better than the status quo on so many levels. The end user experience is smoother (no custom <abbr>MQTT</abbr> setup required, no remembering under which address the <abbr>HTTP</abbr> interface can be found, no guessing if Home Assistant will work with it nor not) and we can finally have true interoperability through standardized interfaces and data semantics. I guess it is just a bit early.


{{ image_credits(author_name="Jakub Zerdzicki", author_url="https://www.pexels.com/@jakubzerdzicki/", img_url="https://www.pexels.com/photo/light-bulbs-surrounding-smartphone-19089175/", site="Pexels") }}


[^name]: The name was removed from the quote since the person spoke for the project and this is not meant as a personal blame.


