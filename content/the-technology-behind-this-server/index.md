+++
title = "The Technology Behind this Server"
date = 2014-04-27

[taxonomies]
categories = ["blog"]
tags = ["technology"]
+++
A short (but now **outdated**) overview over this server.

<!-- more -->

## Host
I stay with a small [*Hetzner*](https://www.hetzner.de/en/) vServer. Their system is based on KVM so you can install a complete customized system including network and file system configuration.

## Operating System
I used Linux for a while and found it perfect for server tasks. As a distribution I chose *Debian* 7 again because the good tradeoff between actuality and stability plus security. You might think that it's a joke that *Debian* is up to date but take a look at *Red Hat* or *CentOS*.

### Certificate
External connections have to be encrypted! So you need a SSL certificate. The easiest choice is a self signed one. This is also the most arrogant choice because you cannot expect that all visitors of your website check the fingerprints and have the technical skills to do so. Furthermore it's a huge problem when you want to provide a secure SMTP service, because other providers might reject emails to your server because they cannot check your certificate while you're offering a secure service. For my last server I used a cacert certificate which mostly suffers from the same problems because cacert isn't trusted by the most software and providers. Otherwise I'm not in the will to pay a bunch of money for this kind of service. Luckily there is [*StartSSL*](https://www.startssl.com/) which enables you to get a certificate for a domain you own. Please notice that you don't get a wildcard which makes it difficult to provide many subdomains. So use paths for the Webserver and the top level domain for all services. This means no mail.foo.com! The certificate is accepted by all major systems.

### SSH
Standard, no root login, keys only (no password authentication), changed port.

### Getty
I use one [*ngetty*](https://packages.debian.org/stable/ngetty) instance instead of single agetty processes to safe some memory and because it's not that important on a virtual machine.

### Fixing Debian
If you ever worked with a distribution which includes systemd in combination with cgroups and journald you'll miss many things while working with the actual Debian version. The first thing is the setup of daemons which are not provided by the package system (*OpenSMTPD*) or aren't really a daemon (*node.js* + *Ghost*). With systemd it's very easy to start, monitor and control these things. With the old init system, it's a hell of hacking with shell scripts. So I decided to use supervisord which is a little bit like systemd but worse. Believe me, logging and memory requirements sucks but it's a good way to start and respawn single daemons including output fetching and user switching.

The second annoying thing is that *Debian* currently doesn't use cgroups like systemd does so you've no really ability to control and check the resources required by the different server parts. The only thing is to accumulate the processes of one user, because most daemons run as a special username for security reasons. But this isn't so easy because for some stupid reason there is no builtin tool to get this data. So you can stay writing scripts or you get [*smem*](https://www.selenic.com/smem/) which is a quite nice piece of software.

I decided against fixing the logging system and stick with the "everyone just spread hundreds of log files and may setup a rotation"-shit.

## Webserver
Instead of the classical overblown Apache webserver I decided to us the lightweight, fast and secure [*nginx*](https://nginx.org/). It's easy to configure and provides all features I need.

### TLS
All connections are redirected to a one domain and the HTTPS protocol. This ensures the best security for all users, including myself. The ciphers are hardened according to [bettercrypto.org](https://bettercrypto.org/)

### Blogging
The first system I used was *Typo3* which provides a great flexibility, but it's slow and complicated and in my opinion not state of the art anymore. My last server was driven by static pages. This has a huge performance advantage but isn't very flexible when you want to write articles when you're on the road. *WordPress* would give me a nice blogging platform while being extensible and easy to use, even on mobile devices. After installing and using it a little bit you recognize that the code is highly crusted and the system slow until you tune it using bunch of plug-ins. So I moved on to a brand new technology and a lightweight and easy to use software: [*Ghost*](https://ghost.org/). It uses node.js which doesn't suffer from stupid and slow process management so you can just start and use it by utilizing nginx as a proxy. Furthermore you don't need to install a database for small sides because *Ghost* uses sqlite. The UI is a bit unstable at the moment, especially on mobile devices, but I think that will change soon. I also like that you write the blog posts using markdown which is very easy and fast.

### User Directories
User Directory Access via public HTML isn't officially supported but you can easy implement a rule that mimes the Apache behavior. Just see [the *Nginx* Wiki](https://wiki.nginx.org/UserDir) for details.

## Jabber
I'm using XMPP/Jabber for years now and it's a great open and decentral chat solution. I have an account with the same address as my email account which makes it easy to contact me. On my last server I was running ejabberd. While there was no specific problem with it, I was looking for something simpler and even more lightweight. Especially the entire erlang dependency suite isn't the best when you're limited in storage. So I decided to use *Prosody*. It's only takes around 1% of the system memory and is super fast (for one user :D ). It is also secured via TLS.

## Mail System
So here comes the most complicated part. I was running a postfix together with *SpamAssassin*, *Postgrey*, *OpenDKIM*, *Dovecot* and *ClamAV* to get mails working and spam out. Sadly, this also keeps the administrator out. *SpamAssassin* and *ClamAV* are slow and the *Postfix* config is the biggest bullshit I've ever seen in this arrea. *Postgrey* fights the spam but also keeps your registration confirmations out which is pretty annoying. So I decided to change everything, except the *Dovecot* server. For this part I only changed the storage location from a daemon specific one to the user directories. Sieve, IMAP, indexing and TLS are still available. So I need new daemons for SMTP and spam protection.

### SMTP
After killing *Postfix* I wanted a piece of software which is reliable, easy to configure, lightweight and secure. I was able to get all of this, let me present you: [*OpenSMTPD*](https://www.opensmtpd.org/). This amazing piece of software coded by the [*OpenBSD*](https://www.openbsd.org/) guys just gave me everthing. OK, I've struggled a bit with the configuration because I missinterpreted some terms but now I'm pretty sure that a understand all aspects of the features I use. Because *OpenSMTPD* is not in the stable repositories yet and there is no backport or usable 3rd party repo, you have to compile it yourself. That's not a big deal if you know some Linux basics.

### Spam Protection
After kicking out *Postgrey* and *SpamAssassin* I wondered how a good spam system should look like. Should it throw out emails because of some weird technical aspects like "contains EXE file", "malformed header", "no dkim signature", "Spanish language", "contains HTML" or "is mentoine on a 3rd party, uncontrolled spam list"? No, I guess not. Because that's not the real problems. This are not mails I have a problem with. My problems are emails with junk content, not-unsubscribable mailing lists, stupid online services, porn advertising and so on. So it's more about the content. Because of this I need an intelligent filter, something that contains machine learning. And I remember that I had something like this a long time ago when I was using 3rd party addresses and POP3. Thunderbird contains a real good junk filter, which, after some training, is pretty releiable. It is based on some Bayes filter technology. And for years there are daemons which do the same. I've picked dspam for some reasons (leightweight, good integration). And until now I'm super happy with it. The only think I had to write on my own was a shell script and a cronjob to train it once per day using one spam folder and all non-spam folders.

### 3rd Party Addresses
The last system was fetching my mails from other accounts using getmail. But because getmail is not able to forward the mails to dspam, I needed another solution. Surprisingly, all my providers (web.de, gmx.net) allow me to forward all incoming mails to my own server. Thats amazing and decouples this accounts pretty well while still enabling me to use my own spam protection (ever tried this weird systems the providers installed? grrrrrr).

### Remaining Parts
I've killed DKIM entierly because the technology neither fight the global spam problem  nor provides it certified mail transport. All daemons communicate using Unix sockets which reduces the overhead and reduces the attack surface. By the way: cutting out the antivirus protection requires me to use a secure system, like I do for years because emails aren't the only threat.

## MySQL
Hehe, just kidding. Why should I need this overblown shitpiece of crep?!

## TODO
For my next server, there are still some things to do:

 - **Switching Hoster:** *Hetzner* isn't very flexible when it comes to virtual machines. It's basically a normal server with less power. (@Hetzner: Welcome in 201X!)
 - **Encrypt Storage:** Using some kind of two stage booting to handle the key-loading. That should make it harder to get information from the machine
 - **Encrypt All Incoming Mails:** While Jabber messages aren't stored on the server (and can protected via OTR), my entire mail archive is located on the server. And *OpenPGP* encryption is uncommon, escpecially for commercial services. So I think I will just encrypt all incoming non-encrypted mails with my public key like [mailbox.org](https://mailbox.org/) is doing.
 - **Reintroduce Web Analytics:** I'm waiting until [Piwik](https://piwik.org/) supports [PostgreSQL](https://www.postgresql.org/) and [HHVM](https://hhvm.com/)
 - **Reduce SSL Key Usage:** Use only one daemon which has access to the private SSL key and works as proxy server for all other services. *Nginx* is a good candidate for this.
