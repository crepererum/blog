+++
title = "Interrail 2015 Part 3"
date = 2015-10-11

[taxonomies]
categories = ["blog"]
tags = ["interrail", "travel"]
+++
This is the third part of the Interrail post series. The two previous parts ([1](https://crepererum.net/interrail-2015-part-1/), [2](https://crepererum.net/interrail-2015-part-1/)) where about Italy and the coast of Slovenia.

<!-- more -->

## Maribor
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9nkfpj/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>
After visiting some coast cities, we decided to see some inland towns. Maribor seemed to be a good choice as one of the biggest wine cities in Slovenia.

{{ image(src="maribor-2.JPG", alt="Maribor 2") }}
It is a very green and not too busy town.

{{ image(src="maribor-1.JPG", alt="Maribor 1") }}
And of course, they have a church.

{{ image(src="maribor-beer.JPG", alt="Maribor Beer") }}
I do not know how, but they brew green beer. It does not taste too bad, but I think the wine here is better than the beer.

{{ image(src="maribor-3.JPG", alt="Maribor 3") }}
They grow a good amount of wine here and many wineries have a good international reputation.

{{ image(src="maribor-4.JPG", alt="Maribor 4") }}
During our second night, we got offered to sleep in a small apartment near the central park, because our hostel was about to be occupied by a football team. Turned out to be a nice place, with a nice visitor the morning after.

{{ image(src="maribor-wine-1.JPG", alt="Maribor Wine 1") }}
After sleeping well it was time for some wine. There is a wine cellar in the middle of town. As far as I remember it is the biggest one in the world located in a town and under actual buildings.

{{ image(src="maribor-wine-2.JPG", alt="Maribor Wine 2") }}
They have some pretty old bottles there, not sure if they are still drinkable.

{{ image(src="maribor-wine-3.JPG", alt="Maribor Wine 3") }}
Not the entire wine is stored in typical wooden barrels. Huge amounts were and stored in tanks made of concrete. They are easier to handle, clean and can be used for a very long time.

{{ image(src="maribor-wine-4.JPG", alt="Maribor Wine 4") }}
Parts of the cellar look more like a scenery for a horror movie.

{{ image(src="maribor-wine-5.JPG", alt="Maribor Wine 5") }}
This is a concrete tank from the inside.

After Maribor we left Slovenia and went to Zagreb, the capital of Croatia.

{{ image(src="dobova.JPG", alt="Zidani Most") }}
On the way we had to change trains in Zidani Most, which is approximately in the middle of nowhere.

## Zagreb
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9n021b/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>
In Croatia, we only visited the capital, Zagreb, and skipped all the beaches. I might come back for them one day, but this trip was about seeing something and not about partying and lying around all day long.

{{ image(src="zagreb-1.JPG", alt="Zagreb 1") }}
The city is nice. Not too busy, not too modern. On the right side you can see the [Cathedral](https://en.wikipedia.org/wiki/Zagreb_Cathedral).

{{ image(src="zagreb-2.JPG", alt="Zagreb 2") }}
[St. Mark's Church](https://en.wikipedia.org/wiki/St._Mark's_Church,_Zagreb)

{{ image(src="zagreb-4.JPG", alt="Zagreb 4") }}
These guys are just everywhere. Here in Zagreb they are quite fun and play local music, but later in Beograd/Belgrade they can get annoying.

{{ image(src="zagreb-5.JPG", alt="Zagreb 5") }}
Inside the cathedral. As I found out later, this is the tomb of [Cardinal Stephanic](https://en.wikipedia.org/wiki/Aloysius_Stepinac).

{{ image(src="zagreb-6.JPG", alt="Zagreb 6") }}
Also a very beautiful building.

After Zagreb, we left Croatia to Beograd/Belgrad, the capital of Serbia and our only non-EU country. Because our interrail tickets where only valid for 10 days, we staid their for 4 ½ days. This amazing city is covered in another post together with some small notes about that trip and Interrail in general.
