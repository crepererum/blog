+++
title = "A Place without Time"
date = 2015-01-14

[taxonomies]
categories = ["blog"]
tags = ["thoughts"]
+++
{{ image(src="cover.jpeg", alt="A Place Without Time") }}

Time -- the only thing we constantly pay but that cannot be charged, grown, created or constructed. It is our most important resource and because it is so important, we try to use it as efficient as possible. We are running from one place to another, hunting for the newest the hippest things, eating faster than fast, messaging in snaps of second, living never ever alone, sleeping short and clean and missing nothing . . . but missing everything.

<!-- more -->

Imagine there is a place where you are free to waste this value resource, a place where time does not matter. Imagine a place where you are not even allowed to count the time. Welcome to my little café. It is a placed specially designed for this rushy world. Feel free to relax and to enjoy your life. Enjoy music, coffee, tea, cookies, newspapers, books and neverending conversations.

So how does it work? When entering the café, you have to pay €2 to show you good will and to prevent cheating. You have to take off your watch and shut down your smartphone, laptop, smartwatch and smartglasses. There is absolutly no signal of time in the room, no clock, no calendar, no TV, no radio. But you are allowed to ask for the time. You call the waiter, also called *~The~Time~Keeper~*. He will charge you €1, because you violate the state without time. Then he will take out a silver pocket watch and will show you and only you what time it is.

Cheating is not allowed of course. If *~The~Time~Keeper~* catches you trying to tell someone what it is or even worse using your watch or phone or another device, he will ask you to leave or pay €3. Should you decide to leave, he will bann you for 1 week. He will forgive you after this week, because that is what a cosy place is about. Should you decide to pay, he will take a big book, called *The_Book_of_RUSH* and will write down your name, so we all remember that you are not able to enjoy the state without time but decided to give it another try.

Now you might wonder why someone would pay for entrance and for the risk to get caught by a creepy guy. The money is used to pay magazines and daily newspapers as well as extending a collection of vinyl records, which gets played according to the choice of the costumers (Thanks to Paul Steglich for this awesome idea). And of course you get inexpensive fresh grinded fair trade coffee in different forms depending on the season and wishes, starting at [*French Press*](https://en.wikipedia.org/wiki/French_press) and [*Syphon Coffee*](https://en.wikipedia.org/wiki/Vacuum_coffee_maker) over [*Chemex®*](https://en.wikipedia.org/wiki/Chemex_Coffeemaker) and [*Turkish Coffee*](https://en.wikipedia.org/wiki/Turkish_coffee) to [*Moka*](https://en.wikipedia.org/wiki/Moka_pot) and [*Espresso*](https://en.wikipedia.org/wiki/Espresso) or exotic [*Cold Brewed*](https://en.wikipedia.org/wiki/Cold_brew) delicacies. You will also find some cookies and special sweeties from all over the world as well as tea and a [public bookcase](https://en.wikipedia.org/wiki/Public_bookcase). The interior would be timeless as well, mostly old big couches, wooden tables and pictures from multiple decades, all of this taken from second hand shops.

~~So should I ever realize this idea, come and visit me and forget about your most important thing: your time.~~
So should Paul and I ever realize this idea, come and visit us and forget about your most important thing: your time.
<br />
**Image: [TIME](https://www.flickr.com/photos/becosky/3304801086) by [becosky](https://www.flickr.com/photos/becosky/), 2008, [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)**
