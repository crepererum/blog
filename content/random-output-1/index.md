+++
title = "Random Output #1"
date = 2015-04-19

[taxonomies]
categories = ["blog"]
tags = ["output"]
+++
This is the first *Random Output* post. This post series will contain random material I want to share without long and complicated descriptions.

<!-- more -->

## Coffee and Roses
Coffee supply up and running again #frenchpress
{{ image(src="coffee-and-roses.jpg", alt="Coffee and Roses") }}

## Biolab on Saturday
2015-04-18 -- a sunny calm Saturday, a small town near #CERN: biolab #yolo kicks off
{{ image(src="biolab-on-saturday.jpg", alt="Biolab on Saturday") }}

## Bob Botanisk
Our new flatmate, write him [an email](mailto:bob.botanisk@gmail.com) or visit him [on Facebook](https://www.facebook.com/profile.php?id=100009202765406)
{{ image(src="bob-botanisk.jpg", alt="Bob Botanisk") }}

<br />
<p style="text-align:center;">
<strong style="display:block;margin-bottom:0.5em;">License:</strong>
<a href="https://creativecommons.org/licenses/by-sa/4.0/">
    <img alt="Creative Commons" src="cc.svg" style="display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" /><img alt="Attribution" src="by.svg" style="margin:0 1em;display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" /><img alt="Share Alike" src="sa.svg" style="display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" />
</a>
</p>
