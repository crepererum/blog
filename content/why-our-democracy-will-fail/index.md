+++
title = "Democracy will fail"
date = 2018-04-20

[taxonomies]
categories = ["blog"]
tags = ["thoughts"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

Democracy --- the great promise of freedom, sanity and happiness that westerns leaders made. It is the way we organize states, federations, unions, communities, forums, families, universities and nearly every organization where humans need to come along. We assume that it is the status quo of living together. The problem is, it is --- at least in its current form --- doomed to fail, and I will explain you why I think so.

<!-- more -->

**This post is heavily inspired by Liberalism. I am working on a follow-up post that illustrates a counter-view from a social perspective.**

## Legislation instead of governance
Many people assume that "democracy" means "government by the people". Most of the times though, we use it to give a group of people, a company or an entire organization a valid reason to govern us. *CrimeInc.* has a [great writeup](https://crimethinc.com/2012/04/29/feature-from-democracy-to-freedom) about this topic. In many parts of Europa and the US this has led to either a ["Deep State"](https://en.wikipedia.org/wiki/State_within_a_state), or a very narrow selection of political parties that are following their own interests instead of the ones of the voters and that are lying about their motives, plans, and involvements in political actions. It also led to a imbalance of powers between the state and its people (e.g. the ban of face covering which affects protesters but not the police, whose people are not even required to carry visible ID numbers).

One may argue that this form of indirect democracy is required because people do not have enough time to vote for every single legislative change and do not have the knowledge and expertise to argue about governmental details. The first arguments is a false implication of direct democracy since it assumes a concrete and naïve implementation. There are multiple other implementations that would fix this problem, like batch voting (voting over packages rather than single changes), rotating voting rights and so on. Furthermore, we already complain that with the increasing automation many will loose their jobs. So we will have enough time for this, if we adapt our narrow view on direct democracy. The second argument --- the incompetence of most voters --- is rigged in two ways. The current set of parliament members is not really educated neither, and when ordinary people are not capable of understanding the work of their own government, it is just a sign of laws and structures being too complex and that our political education failed.

## Equal voting weights
Another common assumption is that "equal rights" means "equal votes", so the following exemplary people all have the same weight for every referendum and election:

- student, 20 years old, from a small university town
- retiree, 80 year old, from an industrial town
- mother/father in parental leave, 40 year old, from village next to the border of another country.

Now imagine the following referendum scenarios.

### Example 1: Climate protection
> Should we invest more in long-term research and climate friendly technologies and start to fade out fossil-fuel-based cars over the next 20 years?

Climate change is an issue that will show its consequences in the upcoming years and is relevant for upcoming generations more than it is for our current ones. This long-term aspect should be reflected in the voting process, since older people could clearly argue that the do not care too much about it. People should not be blamed for voting selfish, since it is our nature and by organizing a referendum we ask them for THEIR opinion, not for something that they think would be a common sense. But for decisions with consequences showing up late, we should ask people that will actually be affected by it, not individuals that will be long gone when the decision made today will be relevant.

This aspect should be considered when organizing polls for political strategies, like Brexit (see [Politico](https://www.politico.eu/article/britains-youth-voted-remain-leave-eu-brexit-referendum-stats/) and [BBC](https://www.bbc.com/news/magazine-36619342) for some data about the generation gap).

### Example 2: Limiting the noise pollution in larger cities
> Should we limit the noise pollution in larger cities by installing more public transport and taxing cars crossing core parts of cities?

It is clear that this change would would have an immediately positive impact on the life of our retiree, but may have an negative effect on the mother/father who wants to go shopping to the next larger city. For the student, this law may play a role later in her life. I will focus on the former two to show why they are not equal.

The main difference between these two subjects is how they interact with the affected area --- the city. One lives there, the other uses it as a resource e.g. for shopping, entertaining, or as an indirect economical resource. There exist and existed larger, but very similar relationships in the world:

- the economy of the western world VS cheap production in Asia
- rich cities VS poor farmers
- Europe VS Africa
- Spain VS Catalonia

The pattern is, that one society gains advantages by exploiting limitations of another part of the world where they do not have to face the direct consequences of their decisions. This is clearly wrong, since it limits the freedom of the victims. Local aspects should be decided by local groups.

### Example 3: Mandatory all-day schools for kids
> Should all-day schools be mandatory for kids from 7 to 15?

Why should our different candidate decide about this question? One example might be, that the father/mother has a child directly affected by the changes, the student may have a child in that position in some years and she might just have started to study education science and the retiree may have many years of experience educating children.

On the other hand, there are contra arguments for everyone of them. Parents are not always the best decision maker when it comes to education (I rather think that its better for many children if their parents do not have any influence on their education), the student may have a very incomplete view and the retiree may have very outdated views.

So the main question is if you rather want experts (e.g. knowledge and experience) or affected people (e.g. freedom and feelings) to decide over these changes. This is a rather fundamental question and there are good reasons for both. What is bad are societies that do not clearly decide on a fixed balance of both, since it results in many populist campaigns, pseudo-scientific arguments, and hate and anger.

### Example 4: Increasing of pensions
> Should the pensions, based on an intergeneration-contract, be increased?

With the generational shift, it becomes harder for the younger generation to pay for pensions, while at the same time the amount per pensioner shrinks. You cannot blame older people for demanding an adequate payment or even too much money, and you cannot blame younger people for not willing to pay overlarge amounts of their incomes into a system which will never ever pay back the sum of their lifelong burden. The problem here is that this imbalance also has an effect on the outcome of an election. The larger the imbalance, the less pensioners get and they higher is their demand for an increase. At the same time, they are also more voters and can overrule every decision about that topic. For the younger generation, the outcome looks dark. Their burden increases while at the same time they have no proper option to vote for and they do not only have to face the immediate consequences but a lifelong strain.

Of course, you could come up with another question: is the inter-generation contract meant to fail? I would say so, but since in todays democracies the question is given by politicians and "experts", a change of the system is an unlikely scenario. It would lack instant gratification but would rather be a heavy, year-long effort for all contracting parties and would result in very bad reputation for the political party in charge.

## Binding of multiple independent voting groups
The examples discussed above were originally designed to show problems of the "same weight" assumption, but they also show that finding weights might be difficult as well (this will also be discussed in the *Social Choice Theory* section later). Therefore, it is obvious to ask why the different groups should actually govern over the specified set of rules, contracts and laws. In reality, we have implemented multiple forms of these bounds and separations, for example:

- **location-based:** people solely vote over the same things because they are located next to each other, e.g. in the same country or city
- **expert-based:** knowledge, experience and wisdom are the key to decide as a group over the same topic, e.g. in companies (C-level, expert groups, ...) or universities
- **time-based:** the time of a "membership" of an individual is key to right to vote, e.g. age limits for elections of the parliament (passive and active rights) or membership times within interest groups and clubs
- **attribute-based:** attributes like gender, race or origin can separate voting groups, e.g. in women's rights groups

These methods are not bad per se, and can make a lot of sense, e.g. for protecting closed groups from irrelevant, external influences. On the other hand, the are the root cause of many fights when one of the following attributes hold:

- **too large:** the group contains too many people with too different interests and tries to force a common view, e.g. for overlarge countries and diverse groups
- **not inclusive:** the people being in charge govern over people not being included, e.g. for colonies, globalization, age restrictions and indirect democracies
- **untrue agreement:** the collective decides on things but members do not agree on the method agreements are made, e.g. democracies that can suppress minorities

These are mostly attributes of forced bounds, i.e. when people should agree over things when they actually should not. The simple, but for many people very unsatisfying solution is to not let groups agree on things. Disagreement is a very valid option and we as an advanced civilization should not only be able to deal with it, but celebrate it.

## Psychology
THE driver of human action is egoism. That might sound harsh and accusing to some, who then may respond with stories of [Mother Teresa](https://en.wikipedia.org/wiki/Mother_Teresa), other famous people, the good uncle they know, or defending arguments why they are such good, selfless beings. But here is the thing: everything we do only is for our own good and if it is not to survive, it is for our pleasure. And yes, helping people gives you pleasure, and even if you dedicate your whole life to others, it is because you get something out of it that you enjoy in its own way. Forced action matches this pattern as well, because victims behave because they fair the consequences of disagreement.^[That does neither mean that people get a pleasure because they are victims nor that forcing people is, morally speaking, an acceptable action.] I think there is nothing wrong with this in general. The moral judgement we attach to actions aligns with the fact if we --- for our very own --- get something out of an action (e.g. does it suit our vision of a quiet, posh neighborhood to spit on the ground) or more generally speaking, if it is good for "us", whoever this group might be. The optimization of welfare might be social and selfless, but since we have learned that working for the groups gives us shelter, food, protection, status, sex and other goodies I argue that being social is just a form of egoism with an indirect reward.

Anyway, coming back to votes and election, the outcome of an election should reflect the (egoistic) opinions of the electorate. The question is: does democracy achieve this? This question was already asked in 1895 in [Gustave Le Bon's](https://en.wikipedia.org/wiki/Gustave_Le_Bon) ["The Crowd: A Study of the Popular Mind"](https://en.wikipedia.org/wiki/The_Crowd:_A_Study_of_the_Popular_Mind). I want you to pay attention to the publication year, which is way before the NS disaster in Europe, because the key point of this publication is that a group of people behaves differently as its parts, and might express completely different opinions than if you would ask each and every person in private. So there you go, the answer to why Europe was covered in blood by people who where on their own no murders, no perverts, no haters; an answer decades before the event and an explanation to many over brutalities of humanity before and after 1895. Does this contradict the assumption of egoistic behavior? No, since people were just social, being part of group.

The essence of this is, at least in my very humble opinion, that you should not mix and mash people together to a large group by bending their mind to follow a common idea. Diversity would have helped, socialists and nationalists balancing each other and being less extreme. The problem though is that fundamental different opinions would not form a working democracy, since members of the society could not even agree on the most basic rules without suppressing the other half.^["half" not being meant in a mathematical sense here, but rather in "the other part of the group consisting of 2 parts"]

## Indirect Votes
After we have seen some psychological issues with democracy, I want to cover some aspect on how elections are organized. To start of, this section points out a fundamental weakness in the way many countries organize their voting system: the district based system, the U.S. federal voting district as one example. Instead of people directly voting for things, they vote for a local representative that then either votes for them or that symbolizes an option. The reasons for introducing such a system might either be the simplification of the whole process (you are not required to organize, count, and verify choices of too many people at the same time and without early disclosure), and you provide your mob some kind of local union and someone they can blame for if their wishes will not be fulfilled.

To illustrate the whole disaster, have a look at the following example of 9 voters, having 2 options red and blue:

{{ image(src="garrymandering_001_o.svg", alt="9 voters") }}

Aggregating the choices gives you 5 red VS 4 blue, so the red party wins. Now we introduce 3 districts separating the population into 3 equally sized groups:

{{ image(src="garrymandering_002_o.svg", alt="9 voters with horizontal split") }}

Assuming that introducing voting districts does not change the opinion of the brains (I am sure it does, but for simplicity let us ignore this for now), and ignoring the fact that additional aggregation and decision step will provoke actions based on Game Theoretical considerations^[e.g. "when I choose this radical candidate, then this will get more media coverage, provoking leaders to change their mind" --- remember BREXIT?], we now get 2 red districts VS 1 blue, red wins again. But as you may have already guessed, this is not the only possible split. Have a look at this one:

{{ image(src="garrymandering_003_o.svg", alt="9 voters with vertical split") }}

Counting again gives you 1 red district VS 2 blue ones, blue wins. This is a tiny example of how choosing districts can affect the outcome of an election. This is also called [Gerrymandering](https://en.wikipedia.org/wiki/Gerrymandering) and if you think this constructed example kinda overstates the problem, have a look at the linked Wikipedia article, use your favorite search engine to find some news about it, have a look at [this genius article](http://www-personal.umich.edu/~jowei/gerrymandering.pdf) that provides some mathematical insights on that topic, visit the interactive site of the [Princeton Gerrymandering Project](http://gerrymander.princeton.edu/), or read [this excellent proof](https://www.optaplanner.org/blog/2016/12/06/HowToBecomeUSPresidentWithLessThanAQuarterOfTheVotes.html) on how to become U.S. president with less than a quarter of the of the votes.

## Social Choice Theory
Now let us focus on the theory behind elections. I know that theoretical views can be quite dry and boring, but I try to give you an easy introduction here. First, we make some assumptions, which you later are free to challenge:

- there are **at least 3 options** to choose from (like political parties, candidates, ways to distribute taxes, state forms, etc.) ^[Some readers may argue that you can encode every list of options into a number of binary decisions, which is indeed right, but since this encoding requires some kind of ordering, you will run into a hole new set of problems brought to you by [Game Theory](https://en.wikipedia.org/wiki/Game_theory). The implications are that organizers of the vote become a players as well and are often able to control the outcome of game.]
- there are **at least 2 voters** allowed to participate in the election
- we are only talking about **direct votes**, no intermediate party is involved, so people directly vote for options (either being political candidates, laws, colors, whatever)

All systems that form voting system somehow limit the way of how people can express their priorities, by limiting the number of parties, the number of votes, the way you can distribute votes, and so on. But the opinion of people is not that easy. Even the most surveys forget one important aspect: there is more than just THE optimal outcome. Most people would surely agree that there are options that are not their favorite, but at that are at least better than the worst possible outcome. Precisely, we now assume that every voter is able to order all possible options in a **priority-based** way, like "candidate A would be the best, candidate B is not great but OK, candidate C is kinda OK, and candidate D would be horrible".

On top of these assumption, there should be some rules that a voting system should follow to be fair, which is a definition you can of course also question:

1. **determinism:** The outcome of the system must not be random choice, but for a fixed group of voters and their priority lists, it must always yield the same result. So rolling a dice is not way to solve this puzzle.
2. **completeness:** The outcome must be a full priority list of all options, so you cannot just yield "candidate A is the winner" or "nobody wins".
3. **universality:** The system must work for all possible groups of voters and options. So you cannot just construct a system that only works for a university with exactly 142 voters and 5 options.
4. **non-dictatorship:** The wishes of all voters must be included, the system cannot just select a single voter who's priority list will be the outcome. That does not mean that voters have equal weights, but that it is possible to have enough voters to overrule the cast of a single voter.
5. **independence of irrelevant alternatives:** That means, if you compare the resulting priorities of two candidates (is X better / equal / worse than Y), than this should only be determined on how the voters ranked this two candidates relatively to another. It must be irrelevant if there was another option and how that was rated. An example would be: There could be large fund for one large project per year. If people prefer "green energy" over "nuclear power stations" is unrelated to the fact of "funds for kindergartens" is on the of possible options or not.
6. **monotonicity:** If any voter decides to increase the priority of a certain option, this should either result in no change of the result or it must improve the priority of the option in the result. Especially, it must not degrade the option. Or in other words: if you vote for someone it should not do any harm to them.
7. **non-imposition:** Every possible outcome must be achievable somehow. You cannot just bypass the voters entirely but always providing the same result.

Think about elections you participated in and if they violate one of these rules. For example, presidential elections in France violate (5), parliamentary elections in Germany violated (6) for a long time (due to the fact that they want to incorporate two voting systems, one where you choose a party, one where you choose a candidate). But even if YOU do not find any weakness, I have bad messages. In 1951, a guy named [Kenneth Arrow](https://en.wikipedia.org/wiki/Kenneth_Arrow) proofed that are it is actually impossible to create such a voting system. This setback for our society is also known as [Arrow's impossibility theorem](https://en.wikipedia.org/wiki/Arrow%27s_impossibility_theorem). (the Wikipedia article actually uses a different set of axioms than I did, but I broke up some of them into smaller chunks to make it easier to follow for the reader)

So here we are: democracy is broken and it can be proven. If you want to learn more about how democratic choices can be messed up, have a look at [Social Choice Theory](https://en.wikipedia.org/wiki/Social_choice_theory) and [Voting Theory](https://en.wikipedia.org/wiki/Category:Voting_theory), an entire branch of science focused on different aspect on how you can extract an opinion out of a group of people.

## Stagnation
Someone may argue that the current system is not that bad and that it works for the majority of people. That reminds me in the arguments we had earlier in history: the tyranny of Christianity in the medieval times worked for many people of you just believed in the system aka god and the church, the slavery in America worked when you were sitting on the right side of the scale, the regime in East Germany was not that bad if you did not want to travel or expressed any "abnormal" or revolutionary thoughts. Sure, the current situation is not THAT bad (at least for now), but not moving forward because many people are somewhat satisfied and lazy is never a good strategy, especially when you consider that humanity has (hopefully) some generations left to evolve.

{{ image_credits(author_name="Randy Colas", author_url="", img_url="https://unsplash.com/photos/TW3dFH_4nEk", site="Unsplash") }}
