+++
title = "Serverless"
date = 2019-03-29

[taxonomies]
categories = ["blog"]
tags = ["technology"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

Some months ago, I got an email from my server provider [Hetzner](https://www.hetzner.de/), telling me that I have to
migrate my server to a new system. When I started with this entire server hosting as a student, I had loads of time and
interest in these things, but nowadays, I just want something that is working. I do not want to care about
[exploding mail standards](@/the-wonderful-world-of-e-mail/index.md), Debian upgrades, server migrations, ever evolving
security rules, backups and so on. So I took this opportunity to shut down my main server entirely and move to some
proper cloud services. This posts illustrates the choices and the pros and cons compared to my old, self-hosted
solution.

<!-- more -->

## Blog
For some time now, I was using [Ghost](https://ghost.org/), which initially was just
[Markdown](https://en.wikipedia.org/wiki/Markdown) + pictures. But over time, their software became more complex and
moved to a new format ([Mobiledoc](https://github.com/bustle/mobiledoc-kit)), resulting in some solid vendor lock-in.
Also, I do not see how a blog that gets new content rarely requires a [Node.js](https://nodejs.org/) server, burning a
solid amount of energy. So I decided to move to a static side engine. My new setup contains of
[Zola](https://www.getzola.org/), which is written in [Rust](https://www.rust-lang.org/) and therefore easy to hack on,
and is hosted by [GitLab Pages](https://gitlab.com/crepererum/blog).

For people facing interested in the conversion path, here is the following converter might be helpful:

{{ gist(url="https://gist.github.com/crepererum/137de687af0f9d8301e907e735d2f516") }}

**Warning:** This only converts Markdown-driven posts and cannot handle Mobidoc. This was not a huge issue for me since I
only had a single post in Mobiledoc, where I manually converted the resulting HTML into Markdown.

One thing that I forgot about was that I had configured my side to use
[HTTP Public Key Pinning (HPKP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Public_Key_Pinning). So just
migrating to GitLab pages and make everything unencrypted did not work out. But just providing key and certificate to
GitLab is not sufficient, since the certificate is issued by [Let's Encrypt](https://letsencrypt.org/), so I need a way
to renew it. After some trail + error, I settled with [dehydrated](https://github.com/lukas2511/dehydrated) and
[lexicon](https://github.com/AnalogJ/lexicon) to issue an ACME DNS-01 challenge.

**Pros:**
- simple data format (just Markdown files and images)
- secure (static sides are just served and cannot be altered)
- GIT-based management

**Cons:**
- lost IPv6 support ([Issue 645](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/645))
- manual certificate renewal

## Mail
So emails are [a quite complicated thing](@/the-wonderful-world-of-e-mail/index.md) to host. On top of that, consuming
your own solution seems easy but mail clients on mobile are a hell. I have used [K-9](https://k9mail.github.io/) for
ages and it is an advanced client, but the [IMAP](https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol)
protocol is bad for mobile where you switch between data and WiFi very often and also drains your battery. Also,
end-to-end encryption is basically unused. To solve these issues, I decided to now use
[Tutanota](https://tutanota.com/) as may mail provider.

**Pros:**
- encryption of ALL incoming mails on server including metadata
- better mobile connection management compared to K9

**Cons:**
- no conversation view ([Issue 6](https://github.com/tutao/tutanota/issues/6))
- no nice GPG/[PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) support anymore
- no nice [S/MIME](https://en.wikipedia.org/wiki/S/MIME) support anymore

## VPN
Sometimes you are using a WiFi or you are in a country that blocks certain websites or services. Also,
[TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) connection in certain unstable data or WiFi
connection are a pain. In that situations, a VPN might help. I have used [OpenVPN](https://openvpn.net/) and later on
[WireGuard](https://www.wireguard.com/) on my server and it worked great. Only problem is that you cannot workaround
certain restrictions using that because the server was hosted in Germany. Now I am using
[Azirevpn](https://www.azirevpn.com/) in WireGuard-mode and I am very happy with them.

**Pros:**
- multiple locations

**Cons:**
- provider COULD introspect data (but Hetzner could in theory do the same)

## Backup Storage
I tried to use the server as a backup store but it turns out that this is quite an expensive solution, so some months
ago I already switched to [Backblaze](https://www.backblaze.com/).

**Pros:**
- unlimited disk space
- works with [Synology](https://www.synology.com/) NAS (although support could be better)
- very good [restic](https://restic.readthedocs.io/) integration

**Cons:**
- non-standard API

## DNS
My contract with Hetzner not only included the server but also a domain including DNS servers from their side. I was not
too happy with their support due to lacking
[DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions) which kinda is a topic
since 2011 (search through [the forum](https://forum.hetzner.com/) for details) and they keep delaying it ever since.
Since multiple other technologies require this features (see pros below), I wanted to switch to another provider for
some months now. After some research I decided for [OVH](https://www.ovh.de/).

One of the biggest pain moving a domain is the transfer. I just thought just using the
[Auth-Code](https://www.icann.org/resources/pages/auth-2013-05-03-en) would be sufficient and that would lead to an
instant transfer (modulo DNS TTL). Turns out you also have to approve the transfer and Hetzner only lets you wait for 5
days to do a passive approval. Also turns out 5 days might actually be 6 and in the end the transfer may occur at a
random point in time. Also, OVH only sets up the DNS zone including all entries when the transfer is complete (or I
missed something), so you will for sure end up with some downtime for EVERYTHING INCLUDING EMAIL! Also, the OVH UI is
kinda horrible at this point since they are not able to show states (e.g. "approved by new owner") properly (see cons
below). In the end, it was a pain but now everything works nicely.

BTW: some providers advice you to lower the [TTL](https://en.wikipedia.org/wiki/Time_to_live#DNS_records) before
transfer but I am actually pretty happy that I did not so many consumers did not see the downtime between the transfers.


**Pros:**
- [DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions)
- [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
- [CAA](https://en.wikipedia.org/wiki/DNS_Certification_Authority_Authorization)
- [Anycast](https://en.wikipedia.org/wiki/Anycast#Domain_Name_System)

**Cons:**
- some French messages / UI elements
- UI seems to be buggy in some places

## Missing Bits
I currently did not convert the following pieces but might just rent a tiny server at
[Scaleway](https://www.scaleway.com/) (they BTW also host ARM64 servers) for the following things:

- [TOR](https://www.torproject.org/) relay
- experiments

One large advantage is that the following things are NOT needed anymore:

- configuration backups
- monitoring and intrusion detection (that actually never was really implemented)
- fail-over solutions (never implemented as well)
- regular and quick security updates (I have used very irregular, manual updates)

{{ image_credits(author_name="Kaushik Panchal", author_url="https://unsplash.com/@kaushikpanchal", img_url="https://unsplash.com/photos/0juC5JIhPks", site="Unsplash") }}
