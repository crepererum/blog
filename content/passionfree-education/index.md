+++
title = "Passion-free Education"
date = 2014-10-18

[taxonomies]
categories = ["blog"]
tags = ["thoughts"]
+++
It starts when you go to school and you realize, that there are some teachers who can to teach you something, and other who fail; that there are kids who like biology, mathematics, and languages, and that there are others who don't. And that these love or hate relationship to a subject can be measured by grades, or that this is at least what they're telling you. You slowly get the idea that instead of helping you to create your own ideas, your individual style and an inner drive, they just rate you according to your ability to produce a plain copy of their visions.

<!-- more -->

If you hoped that this is going to be better and you can develop a free mind after choosing your desired subject of study, you'll fall hard. Instead of showing you how to think and create, they're forcing you to reproduce their knowledge, their opinions. I wished this would be about interesting lectures, but instead they hold material back to get you to their boring linear presentation of endless formulas full of small but important mistakes. When it comes to tests you should work like a perfect machine finishing an awful amount of tasks in just too less time. The material that you've got from the lectures is so bad that you have to use Wikipedia, or YouTube videos of better teachers, or that you have to buy their overpriced books. If there was even a lecture and the institute didn't waste the time of hundreds of students by failing to prepare them.

Some time ago I supervised an exam. Realizing that I'm not able to solve all parts correctly I've spoken to a college. His answer:

> Dude, nobody working for this institute is even able to pass this test, except of the two guys who've created it.

This leads to a bunch of problems. People are cheating to pass the tests, memorizing entire books using [Retalin](https://en.wikipedia.org/wiki/Methylphenidate) without understanding even the fundamentals. They're getting sick from all the stress, taking more and more medicine each year. Ever heard about [Grippostad C](https://www.onmeda.de/therapie/medikamente/grippostad-c-id207054/)? It enables you to work for 2 more days until your body finally resigns. Or they're using cocaine to work all night long or weed to control creativity. People will use mindset even after they've finished primary education, even after work, until they die overworked and unhappy. Our economy starts crumbling because of the lack of innovations and our society gets focused on work instead of the life.

You might think that I'm complaining because I'm a lazy student and that these stories from behind the scenes are lies, but there are also Professors who are awake, like [John C. Beck](http://johncbeck.tumblr.com/post/89890392397/why-im-no-longer-a-professor). Read his story if you don't believe me.

But are their only black sheep in your golden education system? I don't know your story, but I'm sure your're remember this one amazing teacher or professor who was able to inspire you by being different, passionate and creative. For me it was my physics teacher. It was an old guy who've got an amazing knowledge a long time ago and didn't hesitate to expand it. He told us many stories of research and life and motivated, even when the topics where hard and theoretical. We need more of these wonderful passionate teachers.

For all the people who believe in true education instead of grades and reproducing books, I encourage you to do your own research and connect with others. Join communities, travel, give talks. If you give lectures, record, and share them, so students from around the world can follow you presenting your knowledge with passion. Don't be afraid of getting feedback or visiting another rhetoric course to improve, or to visit lectures of your colleges to learn from them. Because this world isn't about being super intelligent or strong or wearing suits and titles -- **this all is about survival of the fittest!**
