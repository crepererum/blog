+++
title = "Rust 2020 - Loose Ends and Small Details"
date = 2019-11-09

[taxonomies]
categories = ["blog"]
tags = ["technology", "rust"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

This is my answer to [call for blogs 2020](https://blog.rust-lang.org/2019/10/29/A-call-for-blogs-2020.html) and
somewhat an update to [my post from last year](@/rust-2019-stabilization-and-ecosystem/index.md). You don't need to read
the old post though, since this post is written in way that it incorporates that content as well.

<!-- more -->

For every topic, I will present my personal favorite parts, split into **Completed** (compared to the post from last
year or was not on the list but is nice anyway), **Open** (existed last year and is still relevant), and **New** (got
relevant or new idea).

## Rust 2021
I will skip over the call for ideas for the Rust 2021 edition, since I currently cannot think of any breaking change
that would require a new Rust edition. I would however not mind a new edition, since the last transition (2015 to 2018)
went really smoothly and the tooling support was great.

## Cargo & Crates.io
The package management and build system made good progress and just work, but if used regularly, some service features are
missing. Implementing these could enhance the overall experience.

### Completed
- **Ability to host your own registry:** You can now
  [use your own registry](https://doc.rust-lang.org/cargo/reference/registries.html) and there are already server-side
  implementation out there ([meuse](https://github.com/mcorbin/meuse),
  [alexandrie](https://github.com/Hirevo/alexandrie)).
- **Better interaction of [docs.rs](https://docs.rs/) and [crates.io](https://crates.io/):** After complaining last year
  that these two services should probably be one, I have got a pretty convincing feedback that this is not a good
  idea due to the fact that docs can contain any HTML and JavaScript potentially interfering with or even hijacking the
  crates.io user session. Also, docs.rs is now part of the official Rust ecosystem and
  [has got an improved infrastructure and a build environment closer to crater](https://blog.rust-lang.org/2019/09/18/upcoming-docsrs-changes.html).
- **Auditing:** It is now
  [required that every crates.io user has a valid email address](https://github.com/rust-lang/crates.io/issues/1620)
  which provides traceability of packages.

### Open
- **Security model / integrity:** We are still missing
  [a nice security model](https://github.com/rust-lang/crates.io/issues/75) and
  [end-to-end integrity of crates](https://github.com/rust-lang/cargo/issues/4768).
- **License issues:** I am still convinced that tools like [cargo-lichking](https://github.com/Nemo157/cargo-lichking) must
  be part of cargo, because users often get licenses wrong or create incompatible combinations.
- **Dependency resolution:** People often wonder why crates are compiled twice in slightly different versions or why a
  simple CLI tool out of the sudden uses 200 dependencies. I think cargo could do a better job explaining these things
  or even try to prevent certain cases (e.g. by pointing out upgrade blockers or by downgrading some crates).

### New
- **Automatic cleanup:** While using cargo, there are a lot of intermediate built results and cache entries created,
  that are currently not cleaned up (see [cargo#6229](https://github.com/rust-lang/cargo/issues/6229)). There are tools
  like [cargo-sweep](https://github.com/holmgr/cargo-sweep), [cargo-cache](https://github.com/matthiaskrgr/cargo-cache),
  and [cargo-trim](https://github.com/iamsauravsharma/cargo-trim), but I think this should be a builtin feature.
- **RustSec by default:** [cargo-audit](https://github.com/RustSec/cargo-audit), the tool to consume the
  [RustSec](https://rustsec.org/) database, should be a builtin feature. Security by default.
- **Debugging:** It would be nice if cargo would just set `RUST_BACKTRACE=1` when running software in non-release mode
  and would offer an integration with a debugger (a la `cargo run --debug` and `cargo test --debug`, similar to `pytest
  --pdb`) by default. I know this is covered by [cargo-with](https://github.com/cbourjau/cargo-with), but I think it
  would be friendlier to newcomers if this is integrated into cargo directly (also see
  [cargo#3670](https://github.com/rust-lang/cargo/issues/3670)).
- **Incomplete features:** See [cargo-hack](https://github.com/taiki-e/cargo-hack).

## Rustdoc
I am a bit disappointed with the rustdoc process lately (although I did not contribute anything, so this
complain is not really fair). While the user-facing design is simple, I think it is lacking some features that are
somewhat expected from a documentation tool.

### Completed
None.

### Open
- **Include external markdown files:** Sadly, the [external doc attribute](https://github.com/rust-lang/rust/issues/44732)
  still is unstable, which makes it quite hard to have a nice README, a changelog, and docs in-sync.
- **Images:** [Image support](https://github.com/rust-lang/rust/issues/32104) is missing from rustdoc forcing users to use
  [ASCII-diagrams](https://github.com/crepererum/pdatastructs.rs/blob/be8a8586ab198c489cf1daf86d7c01bf6fb6eca3/src/filters/quotientfilter.rs#L183-L201),
  base64-encoded images, inline SVGs, or links to GitHub pages as a workaround.
- **Maths:** Rust is used by tech people and I think, often a nicely rendered formula can explain code nicely. With
  [KaTeX](https://katex.org/), there would even be a good, somewhat fast option to support that, especially since
  rustdoc-rendered pages already uses JavaScript.
- **Intra-Doc links:** [Intra-rustdoc links](https://github.com/rust-lang/rust/issues/43466) are still unstable but are
  getting close to stabilization.

### New
None.

## IDE & Dev Tooling
There was a solid amount of progress this year on that side!

### Completed
- **rustup improvements:** I was already quite happy about rustup, but it got
  [some new features](https://blog.rust-lang.org/2019/10/15/Rustup-1.20.0.html) lately.
- **rust-analyzer foundations:** [rust-analyzer](https://github.com/rust-analyzer/rust-analyzer) is in a good
  shape now and on the way to be the goto solution for IDEs.

### Open
- **rust-analyzer improvements:** There are some important things to solve for rust-analyzer, mostly associated types
  and proc macros, but I am sure that this can be done.

### New
None.

## Language & Stdlib
The number of big new features in the language and stdlib is small and I think that's good. The language is getting more
mature. Also see [the section on foundation work](#foundation) for some background why there was a huge amount of work
done even though not all of it is visible to the end users.

### Completed
- **Basic async/await:** It's [finally happening](https://blog.rust-lang.org/2019/09/30/Async-await-hits-beta.html).
  Although I personally don't really need that feature at the moment, I think it is an important addition to the
  language and the ecosystem and will have many powerful application even beyond IO.
- **Better algorithms:** It is nice to see that it is possible to use better algorithms like
  [hashbrown](https://github.com/rust-lang/hashbrown) for the standard library and that all users just magically get an
  performance upgrade.

### Open
- **Never type:** We are getting close is finally solving [rust#35121](https://github.com/rust-lang/rust/issues/35121).
- **Inline assembler:** Recently this popped up again on
  [internals.rust-lang.org](https://internals.rust-lang.org/t/should-we-ever-stabilize-inline-assembly/11211) and I
  think it is important for low-level interfaces, kernels, and embedded.

### New
- **Const generics:** [rust#44580](https://github.com/rust-lang/rust/issues/44580) was not really on the list last year,
  but I think this could be important for many abstractions.
- **FFI unwinding:** [This story](https://github.com/rust-lang/rust/issues/58794) has been open for way to long and also was
  not the nicest community endeavor. I hope this gets settled soon and we can finally close this soundness hole.
- **Empty loop soundness:** Another soundness issue are empty loops and <abbr>LLVM</abbr>
  ([rust#28728](https://github.com/rust-lang/rust/issues/28728)) and I think these problems should be tackled with
  higher priority than new features.

## Docs, Community, Ecosystem
This is kinda a "Misc" section, but contains important notes as well.

### Completed
- **Rustlings, quickstart:** I am usually referring to [rustlings](https://github.com/rust-lang/rustlings) as a good introduction
  for newcomers and when I was looking into it again recently, I was massively surprized to see how much it had
  changed over the last year. 🙂 The same goes for the
  [riscv quickstart guide](https://github.com/riscv-rust/riscv-rust-quickstart) which is a pleasure to work with.
- **Insides:** The [Inside Rust Blog](https://blog.rust-lang.org/inside-rust/index.html) is now available with
  nice information about the ongoing work and the different teams.

### Open
None (section did not exist last year).

### New
- **1.0 crates:** Many important crates currently have a `0.x` version number and per [Semantic
  Versioning](https://semver.org/) are therefore be considered to be somewhat unstable. I think many developers fear
  that once `1.0` is released, they cannot make any fundamental changes anymore and they do not yet want to commit the
  current design. There is no harm in releasing a `2.0` though and I think proper major versions would make it easier
  for crate consumers.
- **Dependency reduction:** It would be nice if developers would choose their dependencies in a slightly more
  restrictive way. Small algorithms do not need to be imported (e.g. leftpad) and some default features are not always
  required. This can lead to massive reductions in binary size and compilation times.

## Foundation Work, Compiler {#foundation}
Some work is not really visible as a feature, but nevertheless important.

### Completed
- **Library extraction:** Libraries like [polonius](https://github.com/rust-lang/polonius),
  [chalk](https://github.com/rust-lang/chalk) and [rustc_lexer](https://crates.io/crates/rustc_lexer) are important
  crates to the compiler (although polonius and chalk are currently not used on stable). They can be used by
  non-compiler crates like rust-analyzer now which is just a massive gift to the ecosystem. Also, these crates
  have seen massive work and will make the compiler even better.
- **Miri:** Although still experimental, [miri](https://github.com/rust-lang/miri) has seen a number of changes and
  is [now available via rustup](https://www.ralfj.de/blog/2019/03/26/miri-as-rustup-component.html), so everyone can use
  it to find nasty bugs in their unsafe code. It is surprising to me that unsafe code can now be checked at
  least somewhat and it is nice to know that "unsafe" does not mean "you are on your own now!".
- **Speed:** The Rust compiler [gets
  faster](https://perf.rust-lang.org/compare.html?start=2019-01-01&end=2019-11-07&stat=wall-time) and the buildsystem
  (Cargo together with rustc) is
  [smarter](https://blog.rust-lang.org/2019/09/26/Rust-1.38.0.html#pipelined-compilation).

### Open
- **Cranelift:** It would be nice if [cranelift](https://github.com/CraneStation/cranelift) could be used for faster
  debug builds. There is [work ongoing](https://github.com/bjorn3/rustc_codegen_cranelift) to do that and I think we
  will see work on that front over the next year.
- **Compiler internals:** MIR-only RLIBs ([rust#38913](https://github.com/rust-lang/rust/issues/38913)) and
  parallel queries ([rust#48685](https://github.com/rust-lang/rust/issues/48685),
  [rust#59667](https://github.com/rust-lang/rust/issues/59667)) are a real long-term effort but will probably advance
  the compiler massively.

### New
None.


## Final Thoughts
I am very happy about the state of Rust and I think 2020 will bring a solid number of improvements. 🎉

{{ image_credits(author_name="Charles Black", author_url="https://unsplash.com/@divewithchuck", img_url="https://unsplash.com/photos/avOWDpaPt_o", site="Unsplash") }}
