+++
title = "About"
date = 2021-07-24

[taxonomies]
categories = ["pages"]
tags = []
+++

This website is not a commercial offering as it does not sell any products or services, does not contain any ads, and
does not participate in any affiliate programs. Therefore it is not commercial offering under the German law
([§ 5 TMG](https://www.gesetze-im-internet.de/tmg/__5.html) and
[§ 6 TMG](https://www.gesetze-im-internet.de/tmg/__6.html)). However
[§ 18 MStV](http://www.luewu.de/docs/gvbl/docs/2377.pdf) might classify this blog as "Telemedien mit
journalistisch-redaktionell gestalteten Angeboten". Therefore I hereby declare myself as responsible for the content:

> Marco Neumann<br/>
> Forsmannstraße 5b<br/>
> 22303 Hamburg<br/>
> Germany<br/>
> <br/>
> blog@crepererum.net

I do not store any user data. No cookies are used.

## Technologies & Licenses
This website is powered by:

| Name            | Description           | License                   | Website                                                                                          |
| --------------- | --------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| Zola            | Static side generator | MIT License               | [`www.getzola.org`](https://www.getzola.org/)                                                                      |
| Even            | Theme                 | MIT License               | [`github.com/getzola/even`](https://github.com/getzola/even)                                                       |
| KaTeX           | Math typesetting      | MIT License               | [`katex.org`](https://katex.org/)                                                                                  |
| Alegreya        | Text font             | SIL Open Font License 1.1 | [`www.huertatipografica.com/en/fonts/alegreya-ht-pro`](https://www.huertatipografica.com/en/fonts/alegreya-ht-pro) |
| Source Code Pro | Code font             | SIL Open Font License 1.1 | [`github.com/adobe-fonts/source-code-pro`](https://github.com/adobe-fonts/source-code-pro)                         |
| Noto Emoji      | Emojis                | SIL Open Font License 1.1 | [`fonts.google.com/noto/specimen/Noto+Emoji`](https://fonts.google.com/noto/specimen/Noto+Emoji)                   |
| GitLab          | CI & Hosting          | ---                       | [`gitlab.com`](https://gitlab.com/)                                                                                |
