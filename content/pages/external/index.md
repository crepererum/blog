+++
title = "External"
date = 2022-02-05

[taxonomies]
categories = ["pages"]
tags = []
+++

Here you find a selection of work that I have published on other sides / media.

## Talks
- [Solving Aristotle's Puzzle -- Evolutionary Algorithms for Your Everyday Task; Rust Hack & Learn Hamburg December 2019](solving_aristotles_puzzle.pdf)
- [Strongly Typed Dataset in a Weakly Typed World; PyCon.DE 2018](pycon_de_2018_marco_neumann.pdf)

## Blog Posts
- [Building a Simple, Pure-Rust, Async Apache Kafka Client; 2022, The New Stack](https://web.archive.org/web/20220305172259/https://thenewstack.io/building-a-simple-pure-rust-async-apache-kafka-client/)
- [Run-length Encoding for Pandas; 2020, BlueYonder Tech Blog](https://web.archive.org/web/20210228150441/https://tech.blueyonder.com/rle-array/)

## University
- [CASINO TIMES: Compression and Similarity Indexing for Time Series; 2016, Master Thesis](master_thesis.pdf)
- [Information Hiding im Smart-Grid; 2014, Seminar Paper (de)](ipd_smartgrid.pdf)
- [GraBaSS: Graph Based Subspace Search; 2013, Bachelor Thesis](bachelor_thesis.pdf)
