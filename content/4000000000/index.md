+++
title = "$4,000,000,000"
date = 2014-02-21

[taxonomies]
categories = ["blog"]
tags = ["sarcasm"]
+++
This week silicon valley produced the biggest deal since months. *Facebook* bought *WhatsApp* for 19 Billion US Dollar. Because it's more difficult to buy things using stocks, let's only talk about the 4 Billion cash. Four Billion - $4,000,000,000. Can you imagine how much money that is? Me either, so have a look at some examples. What can you do with 4 Billion USD?

<!-- more -->

## Research
You might want to do something really big for humanity, or at least something that proves that some of our theories are right. Something like:

 - **LHC** - maybe the biggest machine on earth which has proven that these Higgs things exist
 - **Wendelstein 7-X** - to test a modern fusion reactor (don't try to build an ITER-like reactor, you just don't want to spend so much money ;) )
 - **Human Genome Project** - I know, it's already finished. But in fact, you could imagine how much gene research you can do. For example, you can decode another ape
 - **Hubble Space Telescope** - You could sponsor the instrument to provide amazing insights to other galaxy and help us to understand the history and future of everything, except everything
 - **...** - other stuff like super crazy big X-Ray cannons, even more particle colliders, mammoth clones, space travel stuff, we-measure-everything-climate satellites, really needed HIV research, another cancer (research) lab, ...

## Development Aid
There are kind a bunch of people who need some help, but I guess because they need it for something boring like "surviving" instead of their daily social shitstream, this is not so important. I mean, instead of look-at-this-green-face-photo-filter and annoying-fart-video-autoplay, they need unhip stuff like:

 - **Food** - to post some photos of it (including even-more-30s-photo-filter)
 - **Water** - how else should they get rid of this horrible hangover?! By the way, nice naked pictures from last week
 - **Education** - because nobody understands their status posts
 - **Electricity** - come on, you know why!
 - **...** - totally senseless stuff like protecting tasty kids and women, featuring anti-everything-and-corruption journalism, supporting why-the-hell-independence-initiatives, ...

## Freedom
Buying freedom, hehe, this freaky guy... No, really, as a tech company, you can do a lot:

 - **Encrypted, Uncensored Communication** - for sharing more cat pictures
 - **Live and Free HQ Video Channels** - porn everywhere!
 - **Anonymization** - that would make selling drugs easier man
 - **Encrypted File Sharing** - finally, free brain-free games for everyone
 - **...** - nerdy stuff, who-really-needs-devide-encryption, conspiratorial knowledge, terroristic information leaking, ...

## Neighbors
Never thought about your own country, all that stuff around you while sitting in your Starbucks? Sure, here is why this would be wasting of time:

 - **Young People** - who really wants the next generation of journalists, researchers, politicians or just damn normal suckers?
 - **Culture** - you know, that thing that existed before *Facebook*? Nope, not *Myspace*!
 - **Integration** - ahhh, OK, sorry, social networks already doing that. Mmh...
 - **Poor and Homeless People** - they maybe won't have a smartphone, or a *Facebook* account, but... no, they cannot like your I-am-so-fucking-sexy-photos
 - **...** - apart from it, you might just build a higher wall and buy a nice car with tinted windows...

## ...
Yep, I guess there are more ideas, but who cares if *Facebook* can just buy:

## You
Finally, here is the thing *Facebook* bought - no, the did not buy a crappy messenger app which uses technology of the last century. Even when the *WhatsApp* messenger works hundred times better than this shitty *Facebook Messenger* piece. (Everyone with a non-always-online-connection knows this.) The did not buy address books, because already got it using their own special methods.  They did not even got a good ad selling platform. They've just bought the right to read, store and use your messages, your pictures, your loves, your hates, your ideas, your thoughts, your mistakes, your life - **they've just bought you!**

<br/>

**PS: I don't suggest any development aid organization here. You may want to decide on your own where, who, how and if you want to help.**

**PPS: If I had a comment system here I would ask you for more ideas. Unfortunately I don't have one. So you might just answer me on Twitter using [@crepererum](https://twitter.com/crepererum)**
