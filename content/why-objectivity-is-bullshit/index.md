+++
title = "Why Objectivity is Bullshit"
date = 2013-11-25

[taxonomies]
categories = ["blog"]
tags = ["thoughts"]
+++
Selecting a hostel, accepting a date request, bying the next car, choosing what to eat, donating to a hospital for children in Mali, sleeping longer on Sundays -  all this decisions we make are based on what we expect to achieve and which personal these achievements have. Unfortunately we're not able to know the future, so we need to guess the value of the things we get.

<!-- more -->

To do so, we asks friends for their opinions, read reviews, lookup ratings and study "product informations" (in case of your next date this are details like hair color, shoe size or number of previous partners ;-) ). Let's stick with with the information we get from platform and networks, because it is the most common case. Because reviews and information tables are made for a wide range of people, we try to design them to be "objective". For the most cases this means to pack a huge amount of numbers in bad designed graphics, strip down personal opinions and optimize it for the gray big mass. Who wants to know if the author of a mountain bike review likes the color of screws? Everyone should be able to pick the desired information, weight them according to the personal preferences and form his opinion independently. A nice idea - which is going to fail.

The way we present information in a review, how we order them, the way we describe things, the scales we utilize, the references we select - all these things contain subjectivity. The more we try to be objective the more we remove details, important details. There are many people out there which tend to prioritize things similar then I would do it, people who have the same taste of art, people with similar requirements. So why should we ignore all these useful hints? The question is not how to avoid them, the challenge is to pick the right ones without spending to much effort.

Selecting the right information sources or parts of them seems to be simple. Many companies try to use social networks assuming that our friends have a similar requirement profile. When taking a quick look to my own network I can easily find out, that this is not the case. Of course I know people that dance to the same kind of music but most of my social contacts don't get really impressed by Trance and Progressive, while my music mades mostly don't have the same style of cooking. So how should any system helps you to find the things you might like?

There are two ways to build up the right, global connections: The first one is that you know with which persons you share rating rules in specific fields, e.g. when you met travelers who liked the same hostel you prefer. To know enough persons of this kind is not always possible so we need a second method. It should be possible to detect persons who have the same taste you have. The only, but highly important requirement is that you also publish your opinion because this is required to compare and classify you.

Using these techniques it should be possible to build up a new kind of crowd based rating system which provides individual recommendations. It is up to researches to find and optimize the right methods to build this system and to fight the strongest opponents: the shy and laziness which prevents people from sharing their opinions. And for you as one part of the gray mass: **be subjective!**
