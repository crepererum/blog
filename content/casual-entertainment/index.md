+++
title = "Casual Entertainment"
date = 2013-11-12

[taxonomies]
categories = ["blog"]
tags = ["thoughts"]
+++
I grew up when music was already digital. The first form of music I really used was packed in the form of CDs. But to be  honest, I've never bought a CD. I just found it to expensive, so I've got copies from friends and family members. CDs required you to prepare your playlists wisely, because getting them required time and money and they only were able to keep around 20 songs.

<!-- more -->

Then MP3s hit the market and changed my style of consumption. It got more casual in contrast to a CD, which wasn't enough to hold the variety of styles and bands I used over the day. And it was usage, because it highly depended on my mood and the effect I wanted to archive when listening to music. So I started to build up a collection by sharing with fiends, sucking out _eMule_ and the _Torrent_ network and utilizing sites like _RapidShare_ and _Megaupload_.

But my collection was quiet small compared to that of a good friend of mine, who owned an external hard drive with 600 GB of music. He invested weeks in sorting, fixing and equipping the collection with high quality album covers. But he never consumed the entire set. Many pieces he only listened a few times to. And there was a growing number of songs he never completely played. The reason is that he owned many pieces only to try them or to be able to play them when there is the right moment to do so, e.g. love songs or soundtracks.

As my own collection grows, I recognized the same problems. But before I really hit this point, _Grooveshark_ was built. It provided a new, casual way to enjoy music. Play the kind of music which you want to at the moment you find optimal. Even new releases were instantly available. Music became part of my life. For every situation I was able to find the best kind of sound. Checking out new stuff was incredible easy.

Because the nature decided to do so, I got older and the requirements changed. Instead of cheap usage of music, I want a high quality, good sorted and fair way to enjoy. I got enough low quality MP3s without tags and the ability to give something back to the artist. But I don't want to miss the casual way without blowing a huge amount of money. In Germany it took years to introduce a service that provides this. _Simfy_ was the first one I used. Later I switched to _Spotify_. In addition to the high quality experience, I'm now able to enjoy my music on mobile devices which adapts pretty well to the lifestyle of getting around or listening while being on track or while working and studying. And artists get payed according to what I'm listening to while I'm paying a amount which seems to be fair to me. Over the last months flatrate companies started to add additional features like social networking and recommendations which support the casualty.

So I can say that I'm pretty happy now with the situation for music. Thanks to the providers for the great service. For the future I would like to see the same for movies too. In Germany flatrates are far to expensive and/or provide movies after a too long delay. The same holds for games. Both parts seem to be handled in the USA already so there is hope that the situation in Germany better in the next five years.

And for all users of _Grooveshark_ & Co I recommend to think about giving something back to the artist. This doesn't mean that you have to sign up for a flatrate. You might just buy an album which you really like or visit some live concerts, because you want to enjoy high quality, fresh stuff. But there is definitely a way to do it.
