+++
title = "Interrail 2015 Part 4"
date = 2015-10-13

[taxonomies]
categories = ["blog"]
tags = ["interrail", "travel"]
+++
This is the last part of my Interrail post series. Check out the previous parts as well ([1](https://crepererum.net/interrail-2015-part-1/), [2](https://crepererum.net/interrail-2015-part-2/), [3](https://crepererum.net/interrail-2015-part-3/)).

<!-- more -->

## Beograd / Belgrade
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9ockkp/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Honestly, we did not expect much from Beograd; simply because we did not know anything about it. When you reach the main train station, it seems like you finally reach the end of the trip. It is old, and dead, but green.

{{ image(src="beograd-we.JPG", alt="Beograd We") }}
Did I mention that interrailing is fun? 😉

{{ image(src="beograd-trainstation-1.JPG", alt="Beograd Trainstation") }}
As I said: the train system in this part of Europe is not the same.

### The fortress
To our surprise, there is a huge fortress in the middle of the city.


{{ image(src="beograd-model.JPG", alt="Beograd Model") }}
A model of the fortress.

{{ image(src="beograd-1.JPG", alt="Beograd 1") }}
The outer walls of the fortress. On the river in the background you can see the old ships, which are cafés during the day and clubs during the night.

{{ image(src="beograd-2.JPG", alt="Beograd 2") }}
The inner wall.

{{ image(src="beograd-3.JPG", alt="Beograd 3") }}
Well from the times of the Roman Empire.

{{ image(src="beograd-bunker.JPG", alt="Beograd Bunker") }}
A underground bunker from the cold war. Construction plans for that one were never found.

{{ image(src="beograd-lamps-1.JPG", alt="Beograd Lamps") }}
They need a lot of lamps to enlighten the entire complex during the night. 😉

{{ image(src="beograd-mirror.JPG", alt="Beograd Mirror") }}
They have spend a good amount of effort in museums that illustrate the history of the fortress and the strategic location of the city.

{{ image(src="beograd-german.JPG", alt="Beograd German") }}
My favorite model of some old scary creatures of Serbia. Pay attention to the name!

{{ image(src="beograd-torture.JPG", alt="Beograd Torture") }}
My favorite museum was about torture.

### A large city
But Beograd is not a small city around a fortress, it is literally huge.

{{ image(src="beograd-4.JPG", alt="Beograd 4") }}
The old trams are, apart from too many cars and some buses, the main mean of transport.

{{ image(src="beograd-10.JPG", alt="Beograd 10") }}
The street where our hostel was located.

{{ image(src="beograd-11.JPG", alt="Beograd 11") }}
In general, Beograd is a very green city.

{{ image(src="beograd-12.JPG", alt="Beograd 12") }}
And love is everywhere.

### Leftovers from other centuries
Churches, they have lot of them.

{{ image(src="beograd-5.JPG", alt="Beograd 5") }}
[St. Mark's Church](https://en.wikipedia.org/wiki/St._Mark's_Church,_Belgrade) -- Constructed since ages and still not ready, the biggest Orthodox church of the world.

{{ image(src="beograd-7.JPG", alt="Beograd 7") }}
The [Church of Saint Sava](https://en.wikipedia.org/wiki/Church_of_Saint_Sava) is one of the largest Orthodox churches in the world.

{{ image(src="beograd-8.JPG", alt="Beograd 8") }}
It was ready and looks very impressive.

{{ image(src="beograd-9.JPG", alt="Beograd 9") }}
Strangely it does not seem to work the Christian churches I know.

{{ image(src="beograd-14.JPG", alt="Beograd 14") }}
[A small one](https://sr.wikipedia.org/wiki/%D0%A6%D1%80%D0%BA%D0%B2%D0%B0_%D0%A1%D0%B2%D0%B5%D1%82%D0%B5_%D0%9F%D0%B5%D1%82%D0%BA%D0%B5_%D1%83_%D0%91%D0%B5%D0%BE%D0%B3%D1%80%D0%B0%D0%B4%D1%83) (no English link, sorry).

### The viewpoint
{{ image(src="beograd-15.JPG", alt="Beograd 15") }}
The [Gardoš Tower](https://en.wikipedia.org/wiki/Gardo%C5%A1_Tower) was used by the Austrian Empire to observe the activities of the Ottoman Empire.

{{ image(src="beograd-16.JPG", alt="Beograd 16") }}
Today you have a beautiful view over Belgrade. The river on the left side is the Danube.

{{ image(src="beograd-wall.JPG", alt="Beograd Wall") }}
Even here, love is everywhere. 😃

### Misc
{{ image(src="beograd-danger-1.jpeg", alt="Beograd Danger") }}
Beograd can be a quite dangerous place as well.

{{ image(src="beograd-ship.JPG", alt="Beograd Ship") }}
A rusty ship, I wonder what this thing is still good for.

{{ image(src="beograd-6.JPG", alt="Beograd 6") }}
Nope, these are no lightsabers. It is the museum dedicated to Nicola Tesla's work. What many people don't know: he is from Serbia.

## The end and some tips
From Belgrade we got back by plane, which was the most efficient option in terms of time and fun. Who wants to sit in a train for one hole day just to get back to the normal day-to-day life. Everything worked out fine and we are healthy and happy ☺

To finish this post, here are some random tips for other travelers:

### Tap water
The tap water was drinkable in all visited cities. Although I am used to non-mineral water (I never buy it), I think that the recommendation by some American institutes about bacteria concentration is overrated (that one is used by [isthewatersafetodrink.com](http://isthewatersafetodrink.com/)). It tasted even better than the water in Ferney, France 😉

### Flying with a sore throat
Because the wind in Beograd gave me some a sore throat, landing during the flight back was not fun. The swollen throat prevents the altitude compensation of the ears. The result was heavy pain during the landing and limited hearing capacity of one ear for around 1 ½ days. Nothing serious, but very nasty.

### Interrail
Maybe one of the most interesting questions: Is it worth it? I think yes, but keep in mind that there might be other options (see the bus example).

Also be careful when using the official Interrail app, because it might not include all connections. To my surprise, the app and website of DB (the German train company) is way more detailed, but requires mobile data or a Wi-Fi connection of course.

A word about the obligatory reservations: They are required in France and Italy and the places are sometimes limited, so be quick. On most of the other routes either you do not need one or nobody cares (also confirmed by another long-time traveler).
