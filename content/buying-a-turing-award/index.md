+++
title = "Buying a Turing Award"
date = 2016-06-20

[taxonomies]
categories = ["blog"]
tags = ["sarcasm"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

Master thesis research -- time to rant:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Research paper from 1970, 11 pages, <a href="https://twitter.com/sciencedirect">@sciencedirect</a> still wants you to pay &gt; $35 😶</p>&mdash; Marco Neumann (@crepererum) <a href="https://twitter.com/crepererum/status/740916835693023234">June 9, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Totally unexpected, I got offered some help:

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">We can help you do your homework perfectly.just DM us. <a href="https://twitter.com/crepererum">@crepererum</a></p>&mdash; customwriter (@customwriter851) <a href="https://twitter.com/customwriter851/status/740919600901165057">June 9, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<!-- more -->

OK than... No, I would never accept this offer since I think it is wrong (see "No Cheating" section at the end of that article). But... let's see how good they are:

{{ image(src="dm_1.jpg", alt="DM Dump 1") }}

*(In case you're wondering: The time attributes of the messages do not line up because I've started to take screenshots mostly after I was sure to get the right answers and than for backup reasons new ones after every new message.)*

If I'm not completely mistaken, this should request a [\\(\\mathrm{P} = \\mathrm{NP}\\) proof](https://en.wikipedia.org/wiki/P_versus_NP_problem), since [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) [is Turing-complete](http://www.igblan.free-online.co.uk/igblan/ca/). Fine, that would be a kinda heavy task, but how much would it cost?

{{ image(src="dm_2.jpg", alt="DM Dump 2") }}

Not a bad price for that kind of work. They even tell me how simple it would be:

{{ image(src="dm_3.jpg", alt="DM Dump 3") }}

OK, but I want to be sure they're experts, otherwise they don't understand the topic:

{{ image(src="dm_4.jpg", alt="DM Dump 4") }}

Since that work is probably worth a [Turing Award](https://amturing.acm.org/), I ask how we should deal with that:

{{ image(src="dm_5.jpg", alt="DM Dump 5") }}

OK, good. But... can they also proof the impossible?

{{ image(src="dm_6.jpg", alt="DM Dump 6") }}

So they can find an algorithm that solves [the Halting Problem](https://en.wikipedia.org/wiki/Halting_problem). Neat. The Problem is that multiple people have already proofed that this would be impossible. Too bad.

{{ image(src="dm_7.jpg", alt="DM Dump 7") }}

I guess they're smelling something, but I wrote them an email:

> Hey,
>
> we just had a Twitter DM discussion (my nickname is @crepererum) about
a 100 page thesis on the following task:
>
> "For Conway's Game of Life, design a system that computes for every
arbitrary but fixed initial state in polynomial time (in the boundaries
of the 'alive'-part of the input state) if the execution will ever
finish or not."
>
> You weren't able to convince me that you're able to understand the task
in full detail. Would you mind to do so before I book+pay for any
service?
>
> Cheers,<br />
> Marco Neumann

After some days and at a point where I thought they would never reply they pinged me asking for the status:

{{ image(src="dm_8.jpg", alt="DM Dump 8") }}

Mmh, weird. But since it's a mail it might got lost somewhere. I actually did not expect that they have a problem with it:

{{ image(src="dm_9.jpg", alt="DM Dump 9") }}

Which attachments???

{{ image(src="dm_attachement-1.jpg", alt="DM Attachments") }}

😂 Oh gosh! Yeah, I usually sign my mails using [S/MIME](https://en.wikipedia.org/wiki/S/MIME) (`smime.p7s`) and [PGP/MIME](https://tools.ietf.org/html/rfc3156) (`signature.asc`). If the mail client doesn't support these they will show up as attachments. This was never a problem, even for non-IT people. But I think they are not really happy about the content of the mail as well:

{{ image(src="dm_10.jpg", alt="DM Dump 10") }}

Sadly I did not get an answer. So I guess they won't get me the promised Turing Award 😢

## No Cheating
I choose a topic which is totally unrelated to my actual thesis, just to be sure that nobody out there can say I'm cheating (and the ones who do are actually pretty ~~dump~~ dumb). To prove that I did my thesis work before that article was published, here is the GIT-hash of the current HEAD state which you can verify once my thesis is finished at the end of the semester: `d8e9ed3b0f81ca662890ce16fead158c3c2af15c`. (Update: [GitHub link](https://github.com/crepererum/casino_times/commit/d8e9ed3b0f81ca662890ce16fead158c3c2af15c))

I guess the entire business model of this type of service is entirely based on [a wrong eduction model](@/passionfree-education/index.md) 😐.

<br />

**Image: ["inflating\_turing\_1"](https://www.flickr.com/photos/jonathanmccabe/4309502565/in/photolist-7yPjYn-mn4maH-nckGWM-9b2pcn-7zHpSt-7zHkK4-s3VYtd-5w1DrK-85atyh-B9Lwf-62mvDL-jinRQ-akDo1U-jihZT-jinJj-7Ew7ki-jgQQD-jihhk-jihe1-jihaK-jgQXP-jgPWY-jigTg-jgQ4o-jinMk-jgPfw-jih2v-B9Lqz-jgPna-jgRSN-jigXu-jihqF-qXJ32n-jgRJV-jigxE-jgRmw-jiihM-jgRuM-cgudQo-jinkC-jigNW-jgQHo-jgPwR-jgQAv-jind2-jigJk-9qshHw-jgSnS-jgNZf-7zHnJ2) by Jonathan McCabe, [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/), 2013**
