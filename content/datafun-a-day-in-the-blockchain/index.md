+++
title = "Datafun: A day in the Blockchain"
date = 2015-05-03

[taxonomies]
categories = ["blog"]
tags = ["datafun", "graphics"]
+++
{{ image(src="viz.png", alt="a day in the blockchain") }}

This time there I do not have to tell much about the graphics. The reason for this is that my experiment turned out to be less enlightening than I hoped it would be. Anyway, it is quite a good amount of data and some people might need a new data-driven wallpaper. What you see here is a the transaction split over a day.

<!-- more -->

If you are not familiar with the system bitcoin transactions work:

{{ image(src="transaction.svg", alt="bitcoin transaction") }}

Every transaction is made up from bitcoins you have received earlier. You take the sum of enough transactions and split them into new parts. Even when you only transfer a very small amount to a friend, you have to split the amount of some received transactions and transfer one of the parts to your friend's wallet address. What happens to the remaining piece of the cake you might ask. The software will transfer it where it belongs to -- to yourself. This way you can backtrack and prove all transactions.

Back to the graphics: It shows the total amount that is handled by a transaction (inner ring = lowest amount, outer ring = highest amount, logarithmic scale) and how the value is spend via different outputs (encoded as colourful overlay, see how the cells are kind of split, colour is the same as used for the rings).

Some things that I have learned during this project:

 1. I need a new visualization toolchain. [Python](https://www.python.org/) + [TikZ](http://texample.net/tikz/examples/) + [luatex](http://luatex.org/) + [Ghostscript](https://www.ghostscript.com/) is way too slow, especially the LaTeX part.
 2. The [bitcoin client API (JSON-RPC)](https://en.bitcoin.it/wiki/API_reference_%28JSON-RPC%29) is too slow for analysis of big data parts, especially the block encoding. I kinda hope that the [libbitcoin project](https://libbitcoin.dyne.org/) will evolve to a decent analysis toolchain. For now I am using Python code for parsing and processing the [binary block schema](https://en.bitcoin.it/wiki/Block).
 3. Creation of schemas and UML graphics is still a huge problem. Currently I am using [TikZ](http://texample.net/tikz/examples/) as well, because it creates pixel perfect diagrams (no, [Gnome Dia](https://wiki.gnome.org/Apps/Dia/) and [Inkscape](https://inkscape.org/en/) are not a usable alternatives). The main drawbacks: Error reporting of the LaTeX system and the huge amount of time required to create even simple graphics.
 4. My [blog software (Ghost)](https://ghost.org/) is still not able to serve large images to different devices. Sorry for that, lovely visitors.
 5. If you cannot sleep, do something productive and write a blog post 😜
