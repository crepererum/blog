+++
title = "Linux On Desktop In 2023"
date = 2023-05-22

[taxonomies]
categories = ["blog"]
tags = ["tech"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

I am using Linux in the form of various distributions for probably over 15 years now. After 8 years with my trusted Lenovo Thinkpad X1 Carbon, it was time for a hardware refresh. I got a [framework] which turned out pretty nice. I take this opportunity to also reflect on the current state of Linux on "Desktops".

<!-- more -->


## Upstream First & Fast
<images>
    {{ image(src="fedora-logo.svg", alt="Fedora Logo" class="limit_height") }}
    {{ image(src="archlinux-logo.svg", alt="Arch Linux Logo" class="limit_height") }}
</images>

Whatever Linux distribution you choose, do yourself and all upstream maintainers a favor and use something that releases often (or has rolling releases[^rolling]) and that does not patch upstream software to death. It is an illusion that "old" means "stable" as [Debian] & Co tried to teach us for many years. Sometimes it is hard to keep features and bug fixes separate and backports of fixes to year old software and the resulting bug reports -- both up- and downstream -- are a real waste of developers time. I recommend [Fedora] for the average Linux user or [Arch Linux] for the nerds.


## The "Init Wars" Are Over
{{ image(src="systemd-logo.svg", alt="systemd Logo" class="limit_height") }}

[systemd] has replaced [SysVinit], [Upstart] & Co. It makes many things so much easier and provides features that you would expect from a reasonable operating system[^operating_system]:

- **Service Management:** You list/restart/terminate services w/o relying on a bunch of shell scripts and PID files and hope that none of the processes ever double-fork.
- **Service Boundaries:** Services are properly encapsulated into cgroups with limits, (optional) security contexts, and access limitations.
- **Service Activation:** Services can be started/stopped when you need them. This includes [dbus] endpoints and timers.

[systemd] is normally criticized for being monolithic. I do not share this opinion. It provides a bunch of features that you may or may not use, but that provide proper integration. Some of these features that I do not want to miss on a modern operating system are:

- **Log Management:** No per-service log file at some random location and with yet-another syntax and maybe some custom log rotation. You can finally correlate log messages.
- **<abbr>DNS</abbr>:** [systemd-resolved] offers local <abbr>[DNS]</abbr> resolver that comes with <abbr>[DNSSEC]</abbr> and [split <abbr>DNS</abbr>]. It "just works". There is no need to use [systemd-networkd] for this, use [NetworkManager] instead which is better suited for laptops that connect to different and new networks regularly.
- **Time Synchronization:** You can use [systemd-timesyncd] to sync your computers clock using <abbr>[NTP]</abbr>. No extra software stack required.


## Partitions
Everything is <abbr>[UEFI]</abbr> based nowadays. Use a <abbr>[GPT]</abbr>. You only need two partitions:

1. **EFI System:** <abbr>[FAT32]</abbr>, contains all EFI binaries required to boot. Mounted under `/efi`.
2. **Data:** <abbr>[LUKS2]</abbr>, which gets unlocked during boot (also see [encryption](#encryption)). Contains a [Btrfs] file system. This in turn can have subvolumes and a swap file (if you need it).

You do not need an [fstab] file or kernel parameters to mount the partitions anymore. Just comply with the [Discoverable Partitions Specification] and [systemd-gpt-auto-generator] will automatically discover, decrypt, and mount your partitions.


## Boot
Everything in the new <abbr>[UEFI]</abbr> world is based on <abbr>[PE]</abbr> images. You can pack a "stub" which translates from <abbr>[UEFI]</abbr> to "Linux" (use [systemd-stub]), your kernel, the [initrd], the kernel command line, and an optional splash screen into a [Unified Kernel Image] -- which is a specific format of an <abbr>[PE]</abbr> image.

Technically you do not need a bootloader because the <abbr>[UEFI]</abbr> can boot your [Unified Kernel Image] directly. However, there are still a few good reasons to do:

- **<abbr>[UEFI]</abbr> Memory:** If you need multiple images for multiple kernels (e.g. as a fallback) you would need to register your different images on every update. Since apparently many <abbr>[UEFI]</abbr> mainboards use somewhat cheap persistent memory chips that should not be overwritten too often. So instead it is better to use the file system as a state store.
- **Random Seeds:** You can store a random seed on disk which is then used to seed your kernel during the early boot phase. This improves security.
- **Boot Counting:** The bootloader can count boot attempts and fall back to stable kernel after an update.

[systemd-boot] is a good bootloader that implements all nice features and is a solid but simple player within an <abbr>[UEFI]</abbr> environment.

I advise you to using an [initrd] that comes with [systemd]. This avoids having two completely different system managers (one during boot, one during runtime).


## Security
We need to make a few assumption regarding security on modern systems:

- **Distro:** The Linux distribution in use is trusted. Updates should be provided promptly.
- **Upstream:** All system level packages are trusted.
- **Hardware:** The hardware the system is running on is trusted.

### Secure Boot
Especially the latter point provides us not only with some drawbacks but also with advantages. All laptops nowadays come with a <abbr>[TPM]</abbr> and allow us to use [Secure Boot]. This means that -- even when your laptop was physically accessed -- only trusted software can decrypt your data (see [encryption](#encryption)). This is a very important property. I cannot overstate how important this combination of encryption and a trusted boot environment is. While encryption for sure provides data at rest, not having a trusted environment where you type the decryption key into is somewhat pointless[^lockup_laptop].

There has been some beef with [Microsoft] over [Secure Boot] because they claimed exclusive key access on many systems. Luckily [framework] allows you to install custom keys at basically every layer, so even if you want to secure your Linux installation, you can still dual-boot [Windows] or be sure that [OpROMs] still load.

So the workflow is as follows:

1. Create key to sign your [Unified Kernel Image], likely a "Signature Database (db)" key.
2. Enroll key into <abbr>[UEFI]</abbr>. Using the [framework] system, just copy it to `/efi` and enroll it using the <abbr>[UEFI]</abbr> <abbr>UI</abbr>.
3. Every time your build a new [Unified Kernel Image], sign that with the aforementioned key.
4. Make sure you have a very strong <abbr>[UEFI]</abbr> password.

### Encryption
{{ image(src="luks-logo.png", alt="LUKS Logo" class="limit_height") }}

<abbr>[LUKS2]</abbr> is used to encrypt all critical data. The [Unified Kernel Image] does not contain any secrets. Use [cryptsetup] for setup, it comes with state-of-the-art defaults. You have multiple options on how to decrypt your data:

- **Password:** You use a complex password. Since [cryptsetup] chose proper defaults, this should be reasonably safe.
- **<abbr>[TPM]</abbr> only:** Rely on the hardware to provide the decryption key if [Secure Boot] was successful. Note that allows attackers to boot up the system, so you then need a proper user account password and security vulnerabilities in the booted software may be exploited.
- **<abbr>[TPM]</abbr> & <abbr>PIN</abbr>:** Same as above but requires an additional <abbr>PIN</abbr>[^pin], which prevents booting the system by an attacker. The <abbr>PIN</abbr> is usually shorter than a full-blown password, but the <abbr>[TPM]</abbr> usually implements a lockout mechanism if there are too many failed attempts, which is nice.
- **<abbr>[FIDO2]</abbr> with and without <abbr>PIN</abbr>:** Same as <abbr>[TPM]</abbr> but use an external hardware token instead.

The <abbr>[TPM]</abbr> and <abbr>[FIDO2]</abbr> options require [systemd-cryptenroll].

### Fingerprint Reader
{{ image(src="fprint-logo.png", alt="fprint Logo" class="limit_height") }}

Many laptops come with fingerprint readers. [fprint] allows you to use that if your like and has a good integration with [Gnome].

### Firewall
{{ image(src="firewalld-logo.svg", alt="FirewallD Logo" class="limit_height") }}

In my mind a firewall on a Laptop mostly prevents external systems and attackers from connecting to local services that accidently or by bad design open an unprotected network interface. In an ideal world this would not be required, but it is a good safety net to have. [FirewallD] provides good integration with [NetworkManager] and provides zone management, i.e. you can have different rules for your trusted home network, your office, and an open Wi-Fi.

### USBGuard
{{ image(src="usbguard-logo.svg", alt="USBGuard Logo" class="limit_height") }}

[USBGuard] protects you against [BadUSB]. It has a rudimentary integration with [Gnome], so you get a desktop notification whenever a device was blocked. Sadly there is no "click here to allow new device" button, so currently the protection level is "lockscreen", so all devices that are plugged into a locked system are blocked. This is better than the default behavior of Linux -- e.g. connecting to a random network if a USB ethernet dongle is plugged in.

In my personal experience [USBGuard] often blocks devices that should just work, e.g. Bluetooth. So I had to disable it.

### Secure Software Foundation
Your system software should be designed to sandbox applications as good as possible. This can (and has to be) archived on multiple levels:

- **System Services:** See [systemd](#the-init-wars-are-over).
- **Applications:** See [Flatpak](#flatpak).
- **Resources & <abbr>IPC</abbr>:** See [Wayland](#wayland), and [PipeWire](#pipewire). [D-Bus] and [polkit] help on the <abbr>IPC</abbr> front.


## Wayland
{{ image(src="wayland-logo.svg", alt="Wayland Logo" class="limit_height") }}

<abbr>[X11]</abbr>[^x11] might have been nice back in the 90s, but it is a pile of hack and a security issue -- every application can wiretap everything. [Wayland] is here to clean up the messy display stack ([Mir] tried that as well but lost). Apps that still rely on <abbr>[X11]</abbr> are bridged via [Xwayland]. Screensharing works (see [PipeWire](#pipewire)).

See ["Wayland Limitations"](#wayland-limitations), because not everything is roses at the moment.


## PipeWire
{{ image(src="pipewire-logo.svg", alt="PipeWire Logo" class="limit_height") }}

[PipeWire] replaced [PulseAudio]. It has proper Bluetooth support and is super stable. It also allows <abbr>[JACK]</abbr> clients, so you only need one audio solution. One may wonder why we need an audio daemon and cannot use <abbr>[ALSA]</abbr> directly. There are multiple good reasons for [PipeWire]:

- **Virtual Hardware:** Sometimes your "hardware" is not physically connected to your system. This includes Bluetooth speakers, loopback devices, network streaming devices, etc.
- **Central Controls:** Have a central and easy way to reroute audio, select default devices, mute applications, and set application-specific levels without clicking through menus of every single application.
- **Filters:** Using apps like [EasyEffects] you can add filters to input and output devices like noise cancellation, <abbr>EQ</abbr>, compressors, deessers, etc. For the [framework] laptop, the community offers <abbr>EQ</abbr> presets which improve audio quality significantly.
- **Video & Screensharing:** [PipeWire] is not only for audio but can also manage webcams and screensharing.
- **Sandboxing:** [Flatpak](#flatpak) plays very well with it.


## Flatpak
{{ image(src="flatpak-logo.svg", alt="Flatpak Logo" class="limit_height") }}

[Flatpak] allows application developers to package and distribute (e.g. through [Flathub]) their work without learning yet another package manager or without fighting all your distribution weirdness. Furthermore, [Flatpak] comes with some form of sandboxing, even though this aspect can be improved in the future. The combination of unified packaging and sandboxing makes it easier to consume proprietary software like [Discord], [Steam], or [Zoom]; but I also find myself consuming many open source applications through it. It has become my go-to way to install <abbr>GUI</abbr> applications. Use [Flatseal] to manage permissions.

Currently, [Canonical] tries to push their own format called [Snap]. I wonder if they again overestimate their market position after the [Upstart] and [Mir] disasters.

See ["Flatpak & GPU"](#flatpak-gpu) for current limitations.


## Backups
<images>
    {{ image(src="restic-logo.png", alt="Restic Logo" class="limit_height") }}
    {{ image(src="kopia-logo.svg", alt="Kopia Logo" class="limit_height") }}
</images>

Backups should be standard these days -- not just any backup, but a good backup. To be precise, a backup should have the following properties:

- **Automated:** We are all lazy and forgetful, so backups should run automated in regular intervals.
- **Opt-out:** All files should be automatically included if they are not explicitly excluded. Otherwise, you are going to forget to include new data.
- **Off-site:** A <abbr>[NAS]</abbr> or a hard drive under your bed will not help you when your house is on fire. So store your data at a distant location.
- **Raid-safe:** The physical location of your data should ideally be owned by someone who is disconnected to yourself as good as possible. If the police (or whoever) is raiding your home, you want your backups to be safe.
- **End-to-End Encrypted:** Nobody without credentials should be able to read your data. Encryption makes sure you can include even sensitive information without having to worry. Encryption must include metadata like file names.
- **Incremental:** You do not want to upload / transfer your whole data again every day.
- **Minimal Management:** If it is set up it should just work. Especially the storage location should not require you to perform weekly checkups. This requirement mostly eliminates private servers as storage locations.
- **Snapshots:** There should be multiple snapshots, not only the last one. A virus or yourself may delete or overwrite certain files, and it might take you a while until you realize that.
- **Easy Access:** It should be simple to access individual files or whole directories for a given snapshot. Mounting a virtual file system is a very good way.
- **Compression:** Ideally data is compressed at least to a certain extent to safe transfer and storage costs.

There are programs that handle these requirements: [Restic] and [Kopia]. The latter has the better <abbr>UI</abbr> but I am using [Restic] for a while now, and it just works. You can choose multiple cloud providers for storage. [Backblaze B2] is my personal choice, but there might be better options.


## Power Management & Fan Control
I somewhat feel that not draining your battery, heating up the keyboard to fry eggs, and being able to hear your own voice through the fan noise is still one of the biggest struggles Linux has.

To get a base level of sane behavior, I suggest you install [power-profiles-daemon] which also comes with [Gnome] integration[^tlp]. You can add [powertop] on top to debug the current drainage but also to use its auto tune option for even better battery life.

On top there seem to be a few magic kernel arguments that for some reason are not the default but help a lot: `mem_sleep_default=deep` and `nvme.noacpi=1`.

Now you have all the requirements to be power efficient, but some apps like your browser may still insist on roasting your <abbr>CPU</abbr> instead of your <abbr>GPU</abbr>. Use `intel_gpu_top` from [Intel <abbr>GPU</abbr> Tools] to debug this. [Firefox] for example required the `media.ffmpeg.vaapi.enabled` config to be set to use <abbr>GPU</abbr> decoding.

Even though software and hardware are as power efficient as possible, your fans may still run on full speed. I do not know why the kernel or whatever driver does not handle this by default, but you may use [thermald] with [dptfxtract] to fix that.


## Desktop Environment
<images>
    {{ image(src="gnome-logo.svg", alt="Gnome Logo" class="limit_height") }}
    {{ image(src="kde-logo.svg", alt="KDE Logo" class="limit_height") }}
</images>

Use a [Desktop Environment] that supports the aforementioned technologies and works without requiring editing config files just to change the resolution of an external monitor. [Gnome] and [KDE] are both reasonable choices. In this post I often refer to [Gnome] because this is my daily driver.


## Non-native Software
Sometimes you want to run software that is not available for your distribution or even for Linux. Depending on the software there are a few ways to achieve that.

### Containers
{{ image(src="podman-logo.svg", alt="Podman Logo" class="limit_height") }}

For <abbr>[OCI]</abbr> containers use [Podman]. It can run "rootless", i.e. without requiring a system-wide daemon with root privileges that may backdoor your entire system. It also integrates better with the rest of a modern Linux system than [Docker].

### Windows Games
{{ image(src="steam-logo.svg", alt="Steam Logo" class="limit_height") }}

For [Windows] games your may just use [Steam]. It uses [Proton] which is based on [Wine] and mostly just works.

### Other Windows Software
{{ image(src="bottles-logo.svg", alt="Bottles Logo" class="limit_height") }}

If you have other software that only runs on [Windows] you may try [Bottles]. It is a tool to configure and run [Wine] but easier to use than prefixes, manual hacks, or [winetricks].

### Virtual Machines
{{ image(src="quickemu-logo.svg", alt="Quickemu Logo" class="limit_height") }}

If you need a real [Windows] or even a [macOS] use [Quickemu], which is an easy frontend for <abbr>[QEMU]</abbr>.


## Firmware Updates
{{ image(src="fwupd-logo.svg", alt="fwupd Logo" class="limit_height") }}

There is more to a laptop / desktop than the operating system. The <abbr>[UEFI]</abbr> has a software version, different embedded controllers like <abbr>SSD</abbr> run mini operating systems on their own. Traditionally vendors either provided software updates via [Windows] programs or via [FreeDOS]-based update disks. Recently the <abbr>[LVFS]</abbr> (Linux Vendor Firmware Service) and the client program [fwupd] allow Linux users to update all kinds of embedded controllers and connected devices like webcams. This is a game changer.


## What Does Not Work (Yet)
Here a few things that I tried to get to work, but that did not.

### iwd
[iwd] aims to replace [wpa_supplicant] with a clean implementation and improved kernel interfaces. Sadly on three machines that I tested it on the Wi-Fi connection just stops working after a while with pretty unclear log messages mostly blaming the access point. [wpa_supplicant] however just works, so I keep using that.

### Flatpak & <abbr>GPU</abbr>
If your [Flatpak] application want to use <abbr>[GPGPU]</abbr> interfaces like [OpenCL] or [oneAPI] you are pretty much out of luck at the moment, and you have to use non-sandboxed applications. Interfaces like [OpenGL], [Vulkan], and <abbr>[CUDA]</abbr> seem to work though. This affects for example [Blender] and [darktable].

### Wayland Limitations
Even years after its appearance [Wayland] still has some limitations. Now before we get into them, you may wonder if this is a heavy regression because <abbr>[X11]</abbr> had all of that sorted out. To be honest I do not think it did. It somehow allowed all kinds of features but the way they were implemented was likely more a hack than a proper solution.

First is <abbr>[HDR]</abbr> and color management. The protocols ([color-representation-v1], [color-management-v1]) for that are currently under discussion and reading through all the details makes one aware that the complexity is probably justified and that we hopefully will have a very solid solution soon.[^hdr]

Fractional scaling is when you want to scale your applications to your display, but this conversion is not a whole number. Now you cannot squeeze a fraction of a pixel into another, so this is far from trivial if you want to avoid a super blurry or otherwise broken result. The protocol [fractional-scale-v1] was recently accepted and also merged into [Gnome]/[Mutter] so it should be available soon -- if you ignore legacy <abbr>[X11]</abbr> apps.


## Summary and The Future
The Linux ecosystem provides a solid base of a Desktop operating system and the current projects are moving things into the right direction. I personally like Linux, and it is the right choice for many techy people. However, I think 2023 is **not** "The Year of the Linux Desktop". There are too many gaps compared to what I would call a "state of the art" operating system.

### Hardware Interaction
Distributions are very conservative when it comes to hardware requirements and default configurations. Energy saving could be way more aggressive by default. The fact that hardware-based video decoding is still opt-in for many applications is frustrating. Software is often compiled for processors released decades ago without using new instruction set extensions by default. On the other hand, commercial competitors like [Microsoft] require you to have at least a somewhat up-to-date hardware configuration. I am not saying that Linux has to be equally aggressive, but it should be more progressive.

### Software Delivery
The fact that installing a Linux distribution (with exceptions like [Fedora Silverblue]) requires a package manager is arcane and error prone. We have plenty of disk space nowadays and it would be way more streamlined and secure to use an [immutable installation] / image-based OS -- either via [libostree] or [mkosi].

Applications should be sandboxed via [Flatpak] and if a developer needs some specialized tooling or a build environment that is not provided by the installation, they can still use <abbr>[OCI]</abbr> containers or [Nix].

### Security
The fact that the security measurements discussed in this blog post are not the default is just wild. People often claim that Linux is more secure than [Windows] which might be true, but the bar is actually way higher if you look at a vanilla [Android] or [macOS] for example.


{{ image_credits(author_name="Mariola Grobelska", author_url="https://unsplash.com/@mariolagr", img_url="https://unsplash.com/photos/wkH63fOsWwM", site="Unsplash") }}


[^rolling]: Note that "rolling release" neither means "untested" nor "unreleased". [Arch Linux] has a testing repository and does not ship unreleased software. Also see [dont-ship.it].

[^operating_system]: I will call the whole combination of Linux as a kernel, the boot loader, [systemd], and all system services an "operating system". Many people have different opinions, but I think for "Linux on desktop" this is a reasonable point of view.

[^lockup_laptop]: Some people may argue that you only have to make sure that you never leave your laptop out of sight. If you want to travel, I find this a very unreasonable assumption.

[^pin]: While <abbr>PIN</abbr> stands for "personal identification number", it does not have to be numeric-only. Rather it is a short password that can be typed in via a keyboard.

[^x11]: I treat <abbr>X11</abbr> and X.Org as the same thing here, because they virtually are the same for most people.

[^tlp]: Some people recommend <abbr>[TLP]</abbr> because it is "better" than [power-profiles-daemon]. I could not find a difference and since the latter has [Gnome] integration, I stick to that.

[^hdr]: Also see [report 1](https://emersion.fr/blog/2023/hdr-hackfest-wrap-up/) and [report 2](https://blogs.gnome.org/shell-dev/2023/05/04/vivid-colors-in-brno/).


[ALSA]: https://www.alsa-project.org/wiki/Main_Page
[Android]: https://www.android.com/
[Arch Linux]: https://archlinux.org/
[BadUSB]: https://en.wikipedia.org/wiki/BadUSB
[Backblaze B2]: https://www.backblaze.com/b2/cloud-storage.html
[Blender]: https://www.blender.org/
[Bottles]: https://usebottles.com/
[Btrfs]: https://en.wikipedia.org/wiki/Btrfs
[Canonical]: https://canonical.com/
[color-management-v1]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14
[color-representation-v1]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/183
[CUDA]: https://developer.nvidia.com/cuda-zone
[cryptsetup]: https://gitlab.com/cryptsetup/cryptsetup
[darktable]: https://www.darktable.org/
[dbus]: https://www.freedesktop.org/wiki/Software/dbus/
[D-Bus]: https://www.freedesktop.org/wiki/Software/dbus/
[Debian]: https://www.debian.org/
[Desktop Environment]: https://en.wikipedia.org/wiki/Desktop_environment
[Discord]: https://discord.com/
[Discoverable Partitions Specification]: https://uapi-group.org/specifications/specs/discoverable_partitions_specification/
[DNS]: https://en.wikipedia.org/wiki/Domain_Name_System
[DNSSEC]: https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions
[Docker]: https://www.docker.com/
[dont-ship.it]: https://dont-ship.it/
[dptfxtract]: https://github.com/intel/dptfxtract
[EasyEffects]: https://github.com/wwmm/easyeffects
[FAT32]: https://en.wikipedia.org/wiki/File_Allocation_Table#FAT32
[Fedora]: https://getfedora.org/
[Fedora Silverblue]: https://silverblue.fedoraproject.org/
[FIDO2]: https://gitlab.com/cryptsetup/cryptsetup
[Firefox]: https://www.mozilla.org/en-US/firefox/
[FirewallD]: https://firewalld.org/
[Flathub]: https://flathub.org/
[Flatpak]: https://flatpak.org/
[Flatseal]: https://flathub.org/apps/com.github.tchx84.Flatseal
[fprint]: https://fprint.freedesktop.org/
[fractional-scale-v1]: https://wayland.app/protocols/fractional-scale-v1
[framework]: https://frame.work
[FreeDOS]: https://www.freedos.org/
[fstab]: https://wiki.archlinux.org/title/Fstab
[fwupd]: https://github.com/fwupd/fwupd
[Gnome]: https://www.gnome.org/
[GPGPU]: https://en.wikipedia.org/wiki/General-purpose_computing_on_graphics_processing_units
[GPT]: https://en.wikipedia.org/wiki/GUID_Partition_Table
[HDR]: https://en.wikipedia.org/wiki/High_dynamic_range
[immutable installation]: https://0pointer.net/blog/fitting-everything-together.html
[initrd]: https://en.wikipedia.org/wiki/Initial_ramdisk
[Intel <abbr>GPU</abbr> Tools]: https://gitlab.freedesktop.org/drm/igt-gpu-tools
[iwd]: https://iwd.wiki.kernel.org/
[JACK]: https://jackaudio.org/
[KDE]: https://kde.org/
[Kopia]: https://kopia.io/
[LVFS]: https://fwupd.org/
[libostree]: https://ostreedev.github.io/ostree/
[LUKS2]: https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup
[macOS]: https://en.wikipedia.org/wiki/MacOS
[Microsoft]: https://www.microsoft.com/
[Mir]: https://mir-server.io/
[mkosi]: https://0pointer.net/blog/mkosi-a-tool-for-generating-os-images.html
[Mutter]: https://gitlab.gnome.org/GNOME/mutter
[NAS]: https://en.wikipedia.org/wiki/Network-attached_storage
[NetworkManager]: https://networkmanager.dev/
[Nix]: https://nixos.org/
[NTP]: https://en.wikipedia.org/wiki/Network_Time_Protocol
[OCI]: https://opencontainers.org/
[oneAPI]: https://www.oneapi.io/
[OpenCL]: https://www.khronos.org/opencl/
[OpenGL]: https://www.opengl.org/
[OpROMs]: https://en.wikipedia.org/wiki/Option_ROM#UEFI_Option_ROMs
[PE]: https://en.wikipedia.org/wiki/Portable_Executable
[PipeWire]: https://pipewire.org/
[Podman]: https://podman.io/
[polkit]: https://gitlab.freedesktop.org/polkit/polkit/
[powertop]: https://github.com/fenrus75/powertop
[power-profiles-daemon]: https://gitlab.freedesktop.org/hadess/power-profiles-daemon
[Proton]: https://github.com/ValveSoftware/Proton
[PulseAudio]: https://www.freedesktop.org/wiki/Software/PulseAudio/
[QEMU]: https://www.qemu.org/
[Quickemu]: https://github.com/quickemu-project/quickemu
[Restic]: https://restic.net/
[Secure Boot]: https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot
[Snap]: https://snapcraft.io/
[split <abbr>DNS</abbr>]: https://en.wikipedia.org/wiki/Split-horizon_DNS
[Steam]: https://store.steampowered.com/
[systemd]: https://systemd.io/
[systemd-boot]: https://www.freedesktop.org/software/systemd/man/systemd-boot.html
[systemd-cryptenroll]: https://www.freedesktop.org/software/systemd/man/systemd-cryptenroll.html
[systemd-gpt-auto-generator]: https://www.freedesktop.org/software/systemd/man/systemd-gpt-auto-generator.html
[systemd-networkd]: https://www.freedesktop.org/software/systemd/man/systemd-networkd.service.html#
[systemd-resolved]: https://www.freedesktop.org/software/systemd/man/systemd-resolved.service.html
[systemd-stub]: https://www.freedesktop.org/software/systemd/man/systemd-stub.html
[systemd-timesyncd]: https://www.freedesktop.org/software/systemd/man/systemd-timesyncd.service.html
[SysVinit]: https://en.wikipedia.org/wiki/Init#SysV-style
[TLP]: https://linrunner.de/tlp/index.html
[thermald]: https://github.com/intel/thermal_daemon
[TPM]: https://en.wikipedia.org/wiki/Trusted_Platform_Module
[UEFI]: https://en.wikipedia.org/wiki/UEFI
[Unified Kernel Image]: https://uapi-group.org/specifications/specs/unified_kernel_image/
[Upstart]: https://upstart.ubuntu.com/
[USBGuard]: https://usbguard.github.io/
[Vulkan]: https://www.vulkan.org/
[Wayland]: https://wayland.freedesktop.org/
[Windows]: https://www.microsoft.com/en-us/windows
[Wine]: https://www.winehq.org/
[winetricks]: https://wiki.winehq.org/Winetricks
[wpa_supplicant]: https://w1.fi/wpa_supplicant/
[X11]: https://www.x.org/
[Xwayland]: https://wayland.freedesktop.org/xserver.html
[Zoom]: https://zoom.us/
