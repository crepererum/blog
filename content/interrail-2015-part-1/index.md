+++
title = "Interrail 2015 Part 1"
date = 2015-10-07

[taxonomies]
categories = ["blog"]
tags = ["interrail", "travel"]
+++
It was summer and because of the remaining days off which I got granted during my internship at CERN, it was time for vacations. 2 weeks. Time, which I definitely did not want to spend at one single place. So, a road trip? Mmh, without a proper car, properly not the best idea. Bus traveling? I had this one in South America and it takes too much time for only 2 weeks. So train? If so, how far? Good that there are global passes offered by [Interrail](https://www.interrail.eu/) (non-EU citizens may refer to [Eurail](https://www.eurail.com/)). You get the freedom to use your pass multiple times. We (my travel mate Sabrina and I) chose the 5 times in 10 days pass which is enough for 2 weeks.

<!-- more -->

<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.nl2n8997/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

But what are you going to do with all that freedom? Spain? France? Northern Europe? We wanted to see something else and decided for a south-eastern route.

## Venezia / Venice
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9o2bb1/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Our first stop was Venezia (or Venice, should you prefer the international name). A nice, but very touristic city. It was also, by far, the most expensive one during the entire trip.

{{ image(src="venezia-5.JPG", alt="Venezia 5") }}
It is a very touristic city.

{{ image(src="venezia-7.JPG", alt="Venezia 7") }}
Espresso in cafés around the [Piazza San Marco](https://en.wikipedia.org/wiki/Piazza_San_Marco) would cost you €9. In general the city is quite expensive.

{{ image(src="venezia-6.JPG", alt="Venezia 6") }}
Gondolas are very famous. We did not take a ride, because 30 minutes cost around €80.

{{ image(src="venezia-8.JPG", alt="Venezia 8") }}
The ferries are kinda cheap and provide you a great view over the city.

{{ image(src="venezia-9.JPG", alt="Venezia 9") }}
There are calm places in Venezia as well.

{{ image(src="venezia-10.JPG", alt="Venezia 10") }}
Boat race -- the women class.

{{ image(src="venezia-11.JPG", alt="Venezia 11") }}
Even during night the city is beautiful. There are plenty of restaurants and bars.

{{ image(src="venezia-masks.JPG", alt="Venezia Masks") }}
These mask are one of the traditional things they produce there. They range from cheap plastic models to hand painted pottery versions.

Next we wanted to go to the Slovenian coast. How do you get from Italy to Slovenia? Let's cite the hostel owner in Venezia:

> You Germans are funny. You say “The train connection is awful.”, ha, there is NO train connection!

Yep, he is right. There are absolutely no trains between these two countries. So the train would go all the way north to Austria first and then back to the coast. Twelve hours of sitting around, even if the two coast city Trieste (Italy) and Koper (Slovenia) are only ≈25km of proper roads apart. So we decided to go to Trieste first and then try to find a bus or something else to Koper.

## Trieste
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9ncg3n/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Surprisingly Trieste turned out to be quite nice so we decided to stay one night.

{{ image(src="trieste-1.JPG", alt="Trieste 1") }}
The port area.

{{ image(src="trieste-2.JPG", alt="Trieste 2") }}
The main square.

{{ image(src="trieste-3.JPG", alt="Trieste 3") }}
The [Cattedrale di San Giusto](https://en.wikipedia.org/wiki/Trieste_Cathedral), a very nice small church with a lot of mosaics.

{{ image(src="trieste-door2.JPG", alt="Trieste Door 2") }}
We wanted to visit the [Castello di San Giusto](https://it.wikipedia.org/wiki/Castello_di_San_Giusto) but did not find the entrance for quite some time. How should we know that it is that one? Yes, the elevator there.

{{ image(src="title-1-1.JPG", alt="Trieste 6") }}
From there, you have a nice view over the monuments around there…

{{ image(src="trieste-6.JPG", alt="Trieste 5") }}
…and the city.

{{ image(src="trieste-7.JPG", alt="Trieste 7") }}
The [Teatro Romano](https://it.wikipedia.org/wiki/Teatro_romano_di_Trieste) was discovered when the city with all the big houses was already build, which describes the strange location in the middle of the town.

That was the first blog post in that series. Make sure to also read the upcoming stories about bus travel, wine, cost and inland cities.
