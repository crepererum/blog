+++
title = "Interrail 2015 Part 2"
date = 2015-10-09

[taxonomies]
categories = ["blog"]
tags = ["interrail", "travel"]
+++
This is the second part of my Interrail post series. [The first one](https://crepererum.net/interrail-2015-part-1/) was about Venizia/Venice and Trieste and now we are continuing to Slovenia.

<!-- more -->

## Koper
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.ni9n608l/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Yes, there is a bus from Trieste to Koper and it costs you €3.20 to get their and 45 minutes of your time. What a nice alternative to twelve hours train. It is operated by a company which is not covered by Interrail, so you have to plan+pay this part extra.

{{ image(src="koper-1.JPG", alt="Koper 1") }}
Koper is the only port of Slovenia, but also has some nice places to enjoy the sea water.

{{ image(src="koper-2.JPG", alt="Koper 2") }}
The church in the city center is high enough to see the entire city.

{{ image(src="koper-3.JPG", alt="Koper 3") }}
Just be sure to not climb up or down while these bells are ringing.

{{ image(src="koper-animation.gif", alt="Koper Animation") }}
Or you have enough time to watch the people of Koper walking around.

{{ image(src="koper-4.JPG", alt="Koper 4") }}
Later the city offers you a good amount of bars, restaurants, and fast food places.


### Piran
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.nklod4eg/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Because Koper is not that huge, we decided to use our second day to visit other cities at the cost.

{{ image(src="piran-fun.JPG", alt="Piran Fun") }}
This guy seems to be a very special hero. 😆

{{ image(src="piran-wall.JPG", alt="Piran Wall") }}
Centuries ago the city was protected by a wall against attackers from the inland. Today it is a nice viewpoint.

{{ image(src="piran-view.JPG", alt="Piran View 1") }}
The view over the city while standing on the protective wall.

{{ image(src="piran-church.JPG", alt="Piran Church") }}
This city has a church as well. I think these kinds of buildings will be a fundamental part of our trip.

{{ image(src="piran-model.JPG", alt="Piran Model") }}
A small model of the city.

{{ image(src="piran-view2.JPG", alt="Piran View 2") }}
The city is beautiful, but also very small. In the end way staid there only for a couple of hours.

### Portorož
<iframe width='100%' height='500px' frameBorder='0' src='https://a.tiles.mapbox.com/v4/crepererum.nkloj8lc/attribution.html?access_token=pk.eyJ1IjoiY3JlcGVyZXJ1bSIsImEiOiJjZGU3OTA1MmFmN2QxNmJkOGJmMjg3ODJhODdlYmMxYyJ9.R59PrrGRh8isG8A9OwbWBw'></iframe>

Portorož is another famous cost town their. We do not have any pictures of the city. It is a boring tourist town for people lying on the beach all day long and getting their touristic food during the day and cocktails in the evening.

{{ image(src="koper-trainstation.JPG", alt="Koper Trainstation") }}
Time to leave the coast. The train station of Koper has two platforms, one for a wagon which never leaves the station and a real platform. In general the train network is not that dense like in central Europe.

That was the second part of the Interrail blog series. Make sure to check out the next one as well.
