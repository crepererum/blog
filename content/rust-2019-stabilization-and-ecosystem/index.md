+++
title = "Rust 2019 - Usability and Tools"
date = 2018-12-28

[taxonomies]
categories = ["blog"]
tags = ["technology", "rust"]
+++
{{ image(src="cover.jpeg", alt="cover") }}

This is my personal answer to [the call for Rust 2019 Roadmap blog posts](https://blog.rust-lang.org/2018/12/06/call-for-rust-2019-roadmap-blogposts.html). To make it easier to read, I've separated the post into sections by topic.

<!-- more -->

## Cargo & Crates.io
Overall, Rust has a pretty nice ecosystem when it comes publishing and consuming crates. But I feel that there is work to do to really scale up.
- **Better auditing:** It should be obvious who published a crate ([crates.io#1478](https://github.com/rust-lang/crates.io/issues/1478)) at which point in time. Users should also have the ability to sign their published artifacts. Also see [crates.io#25](https://github.com/rust-lang/crates.io/issues/75) and [cargo#4768](https://github.com/rust-lang/cargo/issues/4768).
- **Ability to host your own registry:** This is desperately needed for some companies to use Rust efficiently. They want to have a registry for their own, private crates as well as the ability to use OSS crates on a whitelist-based level (opt-in after security, license and patent audit). This is similar to what [DevPi](https://www.devpi.net/) is to [PyPi](https://pypi.org/). Work has been done here but the final pieces are missing ([rust#44931](https://github.com/rust-lang/rust/issues/44931)).
- **Better interaction of [docs.rs](https://docs.rs/) and [crates.io](https://crates.io/):** Right now, docs.rs replicates parts of the features that crates.io provides (sometimes better, sometimes worse). There should just be a single side w/ a single interface.
- **License issues:** I think many users and libraries often violate licenses because they don't know better. For example: it is [highly questionable if you can have the Apache 2.0 and GPL 2 license in a single, linked binary](https://janelia-flyem.github.io/licenses.html). Luckily, crates provide metadata about their licences in an [SPDX](https://spdx.org/licenses/)-compatible way. Tools like [cargo-lichking](https://github.com/Nemo157/cargo-lichking) can use this and provide proper feedback. I think this must be a standard feature of every package/library manager.
- **Dependency resolution:** I cannot overstate how happy I am that cargo provides a proper dependency solver. All the pain that I face w/o it in other ecosystems (I'm looking at you C++ and pip-world-Python). But the current algorithm has a heavy drawback: for simple command line utilities, I am often compiling over 100 libraries (2 different versions of the same crate count twice), often finding that the compiled crate versions only differ slightly. It would be awesome if instead of "find the latest versions everywhere" there would be a mode called "prioritize: security, than the number of artifacts to compile, than the latest version".

## Rustdoc
I honestly love rustdoc. I have worked with C++ (no default doctool) and Python (doctool is made up of about 3 layers, executes code and has a very complicated and error-prone markup) and I like that there is a single simple standard tool. But I miss some features that I think would be helpful for many people:
- **Include external markdown files:** I dislike that you have to replicate Readme and Changelog information to have them in your final docs. Also, for longer design / architecture documents it would nice to have them separated from the code. This is WIP ([rust#44732](https://github.com/rust-lang/rust/issues/44732)) since quite a while and it would be nice if we could just ship it.
- **Images:** For [pdatastructs.rs](https://github.com/crepererum/pdatastructs.rs), I am using [a bunch of ASCII-diagrams](https://github.com/crepererum/pdatastructs.rs/blob/be8a8586ab198c489cf1daf86d7c01bf6fb6eca3/src/filters/quotientfilter.rs#L183-L201) now. While I think they are nice for many technical docs, I think some things are better explained with proper images. ([rust#32104](https://github.com/rust-lang/rust/issues/32104))
- **Maths:** Rust is used for many mathematic things out there and I think this deserves that formulas at least get some minimal support. [KaTeX](https://katex.org/) might be a good option.
- **Intra-Doc links:** Please finally ship them ([rust#43466](https://github.com/rust-lang/rust/issues/43466)). It is really annoying to set relative links to the rendered HTML and seems more like a hack than a proper solution.

## Language & Stdlib
The Rust language itself has come very far and I'm quite happy with the current feature set. Sure, their could be more advanced features like const generics ([rust#44580](https://github.com/rust-lang/rust/issues/44580)), but I think the Rust team should focus on stabilization, performance and shipping some old unstable features.
- **Never type:** Fulfill the promise of "abstraction w/o performance impact". It is such a nice. Hopefully to be shipped very soon. ([rust#35121](https://github.com/rust-lang/rust/issues/35121))
- **Inline assembler:** I know this is a very complicated topic and right now there are no plans on how to move on, but I think it is an important feature if we ever want to get embedded systems work stable rust.

## Compiler & Analyzer
We have come far with the compiler, especially if you compare the current version with the 1.0 compiler. We got [MIR](https://blog.rust-lang.org/2016/04/19/MIR.html), a query system, NLL and more. But there are many loose ends that I think are important.
- **Code generation backend:** [LLVM](https://llvm.org/) served Rust well and I think will stay the default way to produce production code for quite a while. But it is slow for quick debug builds. It would be nice if Rust could decouple the backend and be able to use [Cranelift](https://github.com/CraneStation/cranelift) for example.
- **Shift code generation:** MIR-only RLIBs ([rust#38913](https://github.com/rust-lang/rust/issues/38913)) and parallel queries ([rust#48685](https://github.com/rust-lang/rust/issues/48685)) would pave the way to a faster compiler and a more modern architecture.
- **Rust analyzer:** [RLS](https://github.com/rust-lang/rls) is good but not great. It is substandard to what users expect from IDEs nowadays. [People are trying to solve this issue](https://ferrous-systems.com/blog/rust-analyzer-2019/) and I think we need more resources to tackle this in 2019.
- **Better algorithms:** I think we could improve the compiler massively by re-thinking some parts of it and pull some abstract solutions from research in. The query system is one aspect, and [chalk](https://github.com/rust-lang-nursery/chalk) and [polonius](https://github.com/rust-lang-nursery/polonius) are moving the compiler in the right direction.

Overall, I am very happy with Rust, the ecosystem and the community. Thank you for providing some fresh air to the IT world :)

{{ image_credits(author_name="Sean Stratton", author_url="https://unsplash.com/@seanstratton", img_url="https://unsplash.com/photos/yttMVKZqllo", site="Unsplash") }}
