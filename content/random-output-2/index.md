+++
title = "Random Output #2"
date = 2015-06-21

[taxonomies]
categories = ["blog"]
tags = ["output"]
+++
It is time for another *Random Output* post, this time featuring an evidence of unicorns, my flat's garden and money problems.

<!-- more -->

## Unicorns
This path might be be dangerous.
{{ image(src="unicorns.jpg", alt="Unicorns") }}

## Herbs
Because we love cooking, my flatmate and I decided to grow some herbs. They are still small but they are doing well and are growing fast.
{{ image(src="herbs.jpg", alt="Herbs") }}

## Money Problems
Living between two countries with two currencies does not simplify coin management, especially when you do not understand numbers in French.
{{ image(src="the-money-thing.jpg", alt="Money Problems") }}

<br />
<p style="text-align:center;">
<strong style="display:block;margin-bottom:0.5em;">License:</strong>
<a href="https://creativecommons.org/licenses/by-sa/4.0/">
    <img alt="Creative Commons" src="cc.svg" style="display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" /><img alt="Attribution" src="by.svg" style="margin:0 1em;display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" /><img alt="Share Alike" src="sa.svg" style="display:inline-block;left:auto;transform:-ms-transform:none;-webkit-transform:none;transform:none;" />
</a>
</p>
